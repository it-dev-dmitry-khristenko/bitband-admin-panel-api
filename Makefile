include .env

all: | ${APP_ENV}
local: | docker-down docker-build docker-up composer-install docker-migrate docker-settings-sync docker-supervisord-restart


##### composer #####

composer-install:
	docker-compose exec php-fpm composer install

##### npm #####

npm-install:
	docker-compose exec node npm i

npm-run-dev:
	docker-compose exec node npm run dev

##### docker compose #####
docker-build:
	docker-compose build

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

docker-rebuild:
	docker-compose down \
	&& docker-compose up -d --build \
	&& docker-compose exec php-fpm composer install

docker-exec:
	docker-compose exec php-fpm bash

docker-migrate:
	docker-compose exec php-fpm php artisan migrate
	docker-compose exec php-fpm php artisan settings:sync

docker-migrate-refresh:
	docker-compose exec php-fpm php artisan migrate:refresh
	docker-compose exec php-fpm php artisan settings:sync
	docker-compose exec php-fpm php artisan db:seed

docker-ide-helper:
	docker-compose exec php-fpm php artisan ide-helper:generate
	docker-compose exec php-fpm php artisan ide-helper:meta
	docker-compose exec php-fpm php artisan ide-helper:model

docker-settings-sync:
	docker-compose exec php-fpm php artisan settings:sync

docker-supervisord-restart:
	docker-compose exec php-fpm /bin/sh -c '/usr/bin/supervisorctl restart all'
