<?php

use App\Domain\Role\Role;
use Laravel\Lumen\Routing\Router;

/**
 * @var Router $router
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(
    [
        'prefix' => 'auth',
        'as' => 'auth',
    ],
    function (Router $router) {
        $router->post('login', ['as' => 'login', 'uses' => 'AuthController@login']);
        $router->post(
            'refresh-token',
            [
                'as' => 'refreshToken',
                'uses' => 'AuthController@refreshToken'
            ]
        );

        $router->post(
            'forgot-password',
            [
                'as' => 'forgotPassword',
                'uses' => 'AuthController@forgotPassword'
            ]
        );

        $router->post(
            'restore-password',
            [
                'as' => 'restorePassword',
                'uses' => 'AuthController@restorePassword'
            ]
        );
    }
);

$router->group(
    [
        'middleware' => 'apiAuth:' . Role::ROLE_ADMIN
    ],
    function (Router $router) {

        $router->group(
            [
                'prefix' => 'auth',
                'as' => 'auth',
            ],
            function (Router $router) {
                $router->get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
            }
        );

        $router->group(
            [
                'prefix' => 'user',
                'as' => 'user',
            ],
            function (Router $router) {
                $router->get('/me', ['as' => 'profile', 'uses' => 'ProfileController@me']);
                $router->put('/me', ['as' => 'updateProfile', 'uses' => 'ProfileController@updateProfile']);
                $router->put(
                    '/password',
                    [
                        'as' => 'updatePassword',
                        'uses' => 'ProfileController@updatePassword'
                    ]
                );

                $router->post('/avatar', ['as' => 'upload-avatar', 'uses' => 'ProfileController@uploadAvatar']);
                $router->delete('/avatar', ['as' => 'delete-avatar', 'uses' => 'ProfileController@deleteAvatar']);
            }
        );

        $router->group(
            ['prefix' => 'users', 'as' => 'users'],
            function (Router $router) {
                $router->get('/', ['as' => 'index', 'uses' => 'UserController@index']);
                $router->get('/{id}', ['as' => 'show', 'uses' => 'UserController@show']);
                $router->put('/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
                $router->post('/', ['as' => 'store', 'uses' => 'UserController@store']);
                $router->delete('/{id}', ['as' => 'destroy', 'uses' => 'UserController@destroy']);
                $router->put('/{id}/password', ['as' => 'change-password', 'uses' => 'UserController@changePassword']);
            }
        );

        $router->group(
            ['prefix' => 'roles', 'as' => 'roles'],
            function (Router $router) {
                $router->get('/', ['as' => 'index', 'uses' => 'RoleController@index']);
            }
        );

        $router->group(
            [
                'prefix' => 'settings',
                'as' => 'settings'
            ],
            function () use ($router) {
                $router->get(
                    '/groups',
                    [
                        'as' => 'groups',
                        'uses' => 'SettingsController@groups'
                    ]
                );
                $router->get(
                    '/groups/{key}',
                    [
                        'as' => 'get',
                        'uses' => 'SettingsController@settings'
                    ]
                );
                $router->put(
                    '/{key}',
                    [
                        'as' => 'update',
                        'uses' => 'SettingsController@update'
                    ]
                );
            }
        );
    }
);
