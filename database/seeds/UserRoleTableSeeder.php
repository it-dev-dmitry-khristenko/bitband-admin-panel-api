<?php

use App\Domain\Role\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class UserRoleTableSeeder
 */
class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [
                'user_id' => 1,
                'role_id' => Role::ROLE_ADMIN,
            ],
            [
                'user_id' => 2,
                'role_id' => Role::ROLE_ADMIN,
            ],
            [
                'user_id' => 4,
                'role_id' => Role::ROLE_USER,
            ],
        ]);
    }
}
