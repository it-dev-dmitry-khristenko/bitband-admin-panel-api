<?php

declare(strict_types=1);

use App\Domain\User\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;


/**
 * Class FileTableSeeder
 */
class FileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('files')->insert(
            [
                'title' => 'avatar',
                'storage_path' => 'avatar.jpg',
                'ext' => 'jpg',
                'owner_type' => User::class,
                'owner_id' => 4,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        );
    }
}
