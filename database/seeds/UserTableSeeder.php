<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

/**
 * Class UsersTableSeeder
 */
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'test@test.com',
                'first_name' => 'admin',
                'last_name' => 'admin',
                'password' => Hash::make('testpass'),
                'reset_password_code' => null,
                'reset_password_code_expires_at' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'email' => 'super@mlearning.com',
                'first_name' => 'firstname',
                'last_name' => 'lastname',
                'password' => Hash::make('testpass'),
                'reset_password_code' => null,
                'reset_password_code_expires_at' => null,
                'created_at' => (Carbon::now())->addMinute(),
                'updated_at' => (Carbon::now())->addMinute(),
            ],
            [
                'email' => 'reset-password-expired@test.com',
                'first_name' => 'firstname',
                'last_name' => 'lastname',
                'password' => Hash::make('testpass'),
                'reset_password_code' => 'expiredPasswordCode',
                'reset_password_code_expires_at' => (Carbon::now())->subDay(),
                'created_at' => (Carbon::now())->addMinutes(2),
                'updated_at' => (Carbon::now())->addMinutes(2),
            ],
            [
                'email' => 'user@test.com',
                'first_name' => 'firstname',
                'last_name' => 'lastname',
                'password' => Hash::make('testpass'),
                'reset_password_code' => null,
                'reset_password_code_expires_at' => null,
                'created_at' => (Carbon::now())->addMinutes(3),
                'updated_at' => (Carbon::now())->addMinutes(3),
            ],
        ]);
    }
}
