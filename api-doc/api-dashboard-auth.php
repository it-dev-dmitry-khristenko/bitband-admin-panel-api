<?php

/**
 * @api {post} /api/dashboard/auth/login Login
 * @apiVersion 1.0.0
 * @apiName login
 * @apiGroup Dashboard Auth
 *
 * @apiParam {string} email
 * @apiParam {string} password
 *
 * @apiSuccess {object} data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Object} data.attributes attributes
 * @apiSuccess {string} data.attributes.tokenType
 * @apiSuccess {Number} data.attributes.expiresIn
 * @apiSuccess {string} data.attributes.accessToken
 * @apiSuccess {string} data.attributes.refreshToken
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data":
 *           {
 *                "type": "token",
 *                "attributes": {
 *                    "tokenType": "Bearer",
 *                    "expiresIn": 31622400,
 *                    "accessToken": "eyJ0eXAiOiJKV1QiLC...",
 *                    "refreshToken": "def5020063d6a978...",
 *                }
 *           },
 *
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 *
 */

/**
 * @api {post} /api/dashboard/auth/refresh refresh
 * @apiVersion 1.0.0
 * @apiName refresh
 * @apiGroup Dashboard Auth
 *
 * @apiParam {string} refreshToken
 *
 * @apiSuccess {object} data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Object} data.attributes attributes
 * @apiSuccess {string} data.attributes.tokenType
 * @apiSuccess {Number} data.attributes.expiresIn
 * @apiSuccess {string} data.attributes.accessToken
 * @apiSuccess {string} data.attributes.refreshToken
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data":
 *           {
 *                "type": "token",
 *                "attributes": {
 *                    "tokenType": "Bearer",
 *                    "expiresIn": 31622400,
 *                    "accessToken": "eyJ0eXAiOiJKV1QiLC...",
 *                    "refreshToken": "def5020063d6a978...",
 *                }
 *           },
 *
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 *
 */

/**
 * @api {get} /api/dashboard/auth/logout Logout
 * @apiVersion 1.0.0
 * @apiName logout
 * @apiGroup Dashboard Auth
 *
 *
 * @apiUse NO_CONTENT
 * @apiUse Authorization
 * @apiUse INTERNAL_SERVER_ERROR
 *
 */

/**
 * @api {post} /api/dashboard/auth/forgot-password forgot password
 * @apiVersion 1.0.0
 * @apiName forgot password
 * @apiGroup Dashboard Auth
 *
 * @apiParam {string} email
 *
 * @apiSuccess {object} data
 * @apiSuccess {String} data.type Entity type
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data":
 *           {
 *                "type": "user",
 *           }
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 *
 */


/**
 * @api {post} /api/dashboard/auth/restore-password restore password
 * @apiVersion 1.0.0
 * @apiName restore password
 * @apiGroup Dashboard Auth
 *
 * @apiParam {string} code
 * @apiParam {string} password
 * @apiParam {string} passwordConfirmation
 *
 * @apiSuccess {object} data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Object} data.attributes attributes
 * @apiSuccess {string} data.attributes.tokenType
 * @apiSuccess {Number} data.attributes.expiresIn
 * @apiSuccess {string} data.attributes.accessToken
 * @apiSuccess {string} data.attributes.refreshToken
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data":
 *           {
 *                "type": "token",
 *                "attributes": {
 *                    "tokenType": "Bearer",
 *                    "expiresIn": 31622400,
 *                    "accessToken": "eyJ0eXAiOiJKV1QiLC...",
 *                    "refreshToken": "def5020063d6a978...",
 *                }
 *           }
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 *
 */
