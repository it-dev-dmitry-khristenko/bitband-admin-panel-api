<?php

/**
 * @api {get} /api/dashboard/settings/groups Get settings group list
 * @apiVersion 1.0.0
 * @apiName Get settings group list
 * @apiGroup Dashboard Settings
 *
 * @apiSuccess {object[]} data Data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Object} data.attributes Settings group attributes
 * @apiSuccess {String} data.attributes.key Settings group key
 * @apiSuccess {String} data.attributes.title Settings group title
 * @apiSuccess {Integer} data.attributes.order Settings group sort order
 * @apiSuccess {Object} meta Meta
 * @apiSuccess {Integer} meta.totalPages Meta total pages (always all)
 * @apiSuccess {Integer} meta.totalItems Meta total items
 *
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 * @apiUse NotFound
 *
 *  @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": [{
 *          "type": "settingsGroup",
 *          "attributes": {
 *              "title": "Main",
 *              "key": "main"
 *              "order": 1,
 *          }],
 *     }
 */

/**
 * @api {get} /api/dashboard/settings/groups/{key} Get settings by group
 * @apiVersion 1.0.0
 * @apiName Get settings by group
 * @apiGroup Dashboard Settings
 *
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 * @apiUse Paginate
 *
 * @apiSuccess {object[]} data Data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Integer} data.type Entity id
 * @apiSuccess {Object} data.attributes Settings attributes
 * @apiSuccess {String} data.attributes.key Settings key
 * @apiSuccess {String} data.attributes.value Settings value
 * @apiSuccess {String} data.attributes.group Settings group
 * @apiSuccess {String} data.attributes.title Settings title
 * @apiSuccess {String} [data.attributes.description] Settings description
 * @apiSuccess {String} data.attributes.type Edit settings field type
 * @apiSuccess {Object} [data.attributes.option] Edit settings available options
 * @apiSuccess {Boolean} data.attributes.isSecret Settings secret param (if true settings value will be fake)
 * @apiSuccess {String} data.attributes.validationRules Settings edit validation rules
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": [{
 *          "type": "settings",
 *          "id": 1,
 *          "attributes": {
 *              "key": "mail_password",
 *              "value": "******",
 *              "group": "email",
 *              "title": "Mail password",
 *              "description": "Mail service password",
 *              "type": "text",
 *              "option": "",
 *              "isSecret": true,
 *              "validationRules": "nullable"
 *          }],
 *          "meta": {"see pagination response tab"}
 *     }
 */

/**
 * @api {put} /api/dashboard/settings/{key} Update settings
 * @apiVersion 1.0.0
 * @apiName Update settings
 * @apiGroup Dashboard Settings
 *
 * @apiParam {String} [value] Settings value
 *
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 * @apiUse NO_CONTENT
 * @apiUse NotFound
*/
