<?php

/**
 * @api {get} /api/dashboard/roles roles list
 * @apiVersion 1.0.0
 * @apiName roles list
 * @apiGroup Dashboard Roles
 *
 * @apiParam {String} [searchQuery] search query
 * @apiParam {Integer} [page[number]] page number
 * @apiParam {Integer} [page[size]] items per page
 * @apiParam {String} [sortBy] sort by field
 * @apiParam {String="createdAt"} [sort] example: -createdAt desc, createdAt asc
 *
 * @apiSuccess {object[]} data Data
 * @apiUse dashboardRole
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": [
 *          {
 *              "type": "role",
 *              "id": 1,
 *              "attributes": {
 *                  "title": "Admin",
 *                  "createdAt": "2020-01-15 07:30:09",
 *                  "updatedAt": null
 *              }
 *          }
 *       ]
 *    }
 *
 * @apiUse ValidationError
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 *
 */


/**
 * @apiDefine dashboardRole
 * @apiVersion 1.0.0
 *
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {int} data.id Entity id
 * @apiSuccess {Object} data.attributes Role attributes
 * @apiSuccess {string} data.attributes.title Role title
 * @apiSuccess {string} data.attributes.createdAt Role createdAt
 * @apiSuccess {string} data.attributes.updatedAt Role updatedAt
 */
