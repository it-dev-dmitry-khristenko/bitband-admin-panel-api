<?php

/**
 * @api {get} /api/dashboard/app-calls-count App calls count
 * @apiVersion 1.0.0
 * @apiName App calls count
 * @apiGroup Dashboard
 * @apiDescription Endpoints shows total calls count in app for last n days, where n its app_api_calls_days_limit system setting
 *
 * @apiSuccess {object} data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Object} data.attributes attributes
 * @apiSuccess {Number} data.attributes.appCallsNumber App calls count
 * @apiSuccess {Number} data.attributes.daysLimit count of days for which the count of calls should be displayed
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data":
 *           {
 *                "type": "appCalls",
 *                "attributes": {
 *                    "appCallsCount": 288,
 *                    "daysLimit": 8,
 *                }
 *           }
 *       }
 *    }
 *
 * @apiUse INTERNAL_SERVER_ERROR
 */
