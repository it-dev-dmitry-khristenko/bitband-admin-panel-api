<?php

/**
 * @api {get} /api/dashboard/users users list
 * @apiVersion 1.0.0
 * @apiName users list
 * @apiGroup Dashboard Users
 *
 * @apiParam {String} [searchQuery] search query
 * @apiParam {Integer} [page[number]] page number
 * @apiParam {Integer} [page[size]] items per page
 * @apiParam {Integer} [status] show active or inactive users
 * @apiParam {String="createdAt", "updatedAt"} [sort] example: -createdAt desc, createdAt asc
 *
 * @apiSuccess {object[]} data Data
 * @apiUse dashboardUser
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": [
 *          {
 *              "type": "user",
 *              "id": 1,
 *              "attributes": {
 *                  "firstName": "Sally",
 *                  "lastName": "Stark",
 *                  "email": "lromaguera@yahoo.com",
 *                  "isActive": true,
 *                  "createdAt": "2020-01-15 07:30:09",
 *                  "updatedAt": null
 *              },
 *              "relationships": {
 *                  "avatar": null,
 *                  "roles": [
 *                      {
 *                          "data": {
 *                          "type": "role",
 *                          "id": 1,
 *                          "attributes": {
 *                              "title": "Admin",
 *                              "createdAt": "2020-01-14 07:30:08",
 *                              "updatedAt": null
 *                          }
 *                      }
 *                  ]
 *              }
 *          }
 *       ]
 *    }
 *
 * @apiUse ValidationError
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 *
 */

/**
 * @api {get} /api/dashboard/users/:id user by id
 * @apiVersion 1.0.0
 * @apiName user by id
 * @apiGroup Dashboard Users
 *
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *           "type": "user",
 *           "id": 1,
 *           "attributes": {
 *               "firstName": "Sally",
 *               "lastName": "Stark",
 *               "email": "lromaguera@yahoo.com",
 *               "isActive": true,
 *               "createdAt": "2020-01-15 07:30:09",
 *               "updatedAt": null
 *           },
 *           "relationships": {
 *               "avatar": null,
 *               "roles": [
 *                   {
 *                       "data": {
 *                       "type": "role",
 *                       "id": 1,
 *                       "attributes": {
 *                           "title": "Admin",
 *                           "createdAt": "2020-01-14 07:30:08",
 *                           "updatedAt": null
 *                       }
 *                   }
 *               ]
 *           }
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse NotFound
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 *
 */

/**
 * @api {post} /api/dashboard/users store dashboard user
 * @apiVersion 1.0.0
 * @apiName store user
 * @apiGroup Dashboard Users
 *
 * @apiParam {string} email user email
 * @apiParam {integer[]} roleIds user role ids
 * @apiParam {string} [firstName] user first name
 * @apiParam {string} [lastName] user last name
 * @apiParam {bool} [isActive] user activity
 *
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *           "type": "user",
 *           "id": 1,
 *           "attributes": {
 *               "firstName": "Sally",
 *               "lastName": "Stark",
 *               "email": "lromaguera@yahoo.com",
 *               "isActive": true,
 *               "createdAt": "2020-01-15 07:30:09",
 *               "updatedAt": null
 *           },
 *           "relationships": {
 *               "avatar": null,
 *               "roles": [
 *                   {
 *                       "data": {
 *                       "type": "role",
 *                       "id": 1,
 *                       "attributes": {
 *                           "title": "Admin",
 *                           "createdAt": "2020-01-14 07:30:08",
 *                           "updatedAt": null
 *                       }
 *                   }
 *               ]
 *           }
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 *
 */

/**
 * @api {put} /api/dashboard/users update dashboard user
 * @apiVersion 1.0.0
 * @apiName update user
 * @apiGroup Dashboard Users
 *
 * @apiParam {string} email user email
 * @apiParam {string} [firstName] user first name
 * @apiParam {string} [lastName] user last name
 * @apiParam {bool} [isActive] user activity
 *
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *           "type": "user",
 *           "id": 1,
 *           "attributes": {
 *               "firstName": "Sally",
 *               "lastName": "Stark",
 *               "email": "lromaguera@yahoo.com",
 *               "isActive": true,
 *               "createdAt": "2020-01-15 07:30:09",
 *               "updatedAt": null
 *           },
 *           "relationships": {
 *               "avatar": null,
 *               "roles": [
 *                   {
 *                       "data": {
 *                       "type": "role",
 *                       "id": 1,
 *                       "attributes": {
 *                           "title": "Admin",
 *                           "createdAt": "2020-01-14 07:30:08",
 *                           "updatedAt": null
 *                       }
 *                   }
 *               ]
 *           }
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 *
 */

/**
 * @api {delete} /api/dashboard/users/{id} Delete user
 * @apiVersion 1.0.0
 * @apiName Delete user
 * @apiGroup Dashboard Users
 *
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 * @apiUse NO_CONTENT
 * @apiUse NotFound
 */

/**
 * @api {put} /api/dashboard/users/{id}/password Change user password
 * @apiVersion 1.0.0
 * @apiName Change user password
 * @apiGroup Dashboard Users
 *
 * @apiParam {String} [password] New user password
 *
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *           "type": "user",
 *           "id": 1,
 *           "attributes": {
 *               "firstName": "Sally",
 *               "lastName": "Stark",
 *               "email": "lromaguera@yahoo.com",
 *               "isActive": true,
 *               "createdAt": "2020-01-15 07:30:09",
 *               "updatedAt": null
 *           },
 *           "relationships": {
 *               "avatar": null,
 *               "roles": [
 *                   {
 *                       "data": {
 *                       "type": "role",
 *                       "id": 1,
 *                       "attributes": {
 *                           "title": "Admin",
 *                           "createdAt": "2020-01-14 07:30:08",
 *                           "updatedAt": null
 *                       }
 *                   }
 *               ]
 *           }
 *       }
 *    }
 *
 * @apiUse Success
 * @apiUse ValidationError
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse Authorization
 */

/**
 * @apiDefine dashboardUser
 * @apiVersion 1.0.0
 *
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {int} data.id Entity id
 * @apiSuccess {Object} data.attributes User attributes
 * @apiSuccess {string} data.attributes.firstName User name
 * @apiSuccess {string} data.attributes.lastName User name
 * @apiSuccess {string} data.attributes.email User email
 * @apiSuccess {string} data.attributes.isActive Is User active
 * @apiSuccess {string} data.attributes.createdAt User createdAt
 * @apiSuccess {string} data.attributes.updatedAt User updatedAt
 *
 * @apiSuccess {Object} data.relationships dashboard User relationships
 *
 * @apiSuccess {Object} data.relationships.roles dashboard User roles
 * @apiSuccess {Object[]} data.relationships.roles.data Role resource data
 * @apiSuccess {String} data.relationships.roles.data.type Entity type
 * @apiSuccess {Number} data.relationships.roles.data.id Entity id
 * @apiSuccess {Object} data.relationships.roles.data.attributes Role resource attributes
 * @apiSuccess {String} data.relationships.roles.data.attributes.title Role resource name
 * @apiSuccess {String} data.relationships.roles.data.attributes.createdAt Role created at data
 * @apiSuccess {String} data.relationships.roles.data.attributes.updatedAt Role updated at data
 *
 * @apiSuccess {Object} data.relationships.avatar User avatar
 * @apiSuccess {Object} [data.relationships.avatar.data] Avatar data
 * @apiSuccess {String} [data.relationships.avatar.data.type] Entity type
 * @apiSuccess {Number} [data.relationships.avatar.data.id] Avatar id
 * @apiSuccess {Object} [data.relationships.avatar.data.attributes] Avatar attributes
 * @apiSuccess {Object} data.relationships.avatar.data.attributes.title Avatar title
 * @apiSuccess {Object} data.relationships.avatar.data.attributes.storagePath Avatar storagePath
 */
