<?php

/**
 * @api {get} /api/dashboard/user/me me
 * @apiVersion 1.0.0
 * @apiName me
 * @apiGroup Dashboard User Profile
 *
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *         "data": {
 *              "type": "user",
 *               "id": 1,
 *               "attributes": {
 *                   "email": "test@test.com",
 *                   "firstName": "test",
 *                   "lastName": "test",
 *                   "createdAt": "2020-01-18 10:35:07",
 *                   "updatedAt": "2020-01-18 10:35:07"
 *               },
 *               "relationships": {
 *                  "avatar": {
 *                        "data": {
 *                            "type": "file",
 *                            "id": 1,
 *                            "attributes": {
 *                                "storagePath": "http:/site.com/avatar/test/logo.jpg",
 *                                "title": "avatar",
 *                            }
 *                        }
 *                 },
 *                 "roles": [
 *                   {
 *                       "data": {
 *                          "type": "role",
 *                          "id": 1,
 *                          "attributes": {
 *                              "title": "Admin",
 *                              "createdAt": "2020-01-14 07:30:08",
 *                              "updatedAt": null
 *                          }
 *                      }
 *                   }
 *                 ]
 *               }
 *           }
 *      }
 *
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse Authorization
 */

/**
 * @api {put} /api/dashboard/user/me update profile
 * @apiVersion 1.0.0
 * @apiName update profile
 * @apiGroup Dashboard User Profile
 *
 * @apiParam {String} [firstName] user first name
 * @apiParam {String} [lastName] user last name
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *              "type": "user",
 *               "id": 1,
 *               "attributes": {
 *                   "email": "test@test.com",
 *                   "firstName": "test",
 *                   "lastName": "test",
 *                   "createdAt": "2020-01-18 10:35:07",
 *                   "updatedAt": "2020-01-18 10:35:07"
 *               },
 *               "relationships": {
 *                  "avatar": {
 *                        "data": {
 *                            "type": "file",
 *                            "id": 1,
 *                            "attributes": {
 *                                "storagePath": "http:/site.com/avatar/test/logo.jpg",
 *                                "title": "avatar",
 *                            }
 *                        }
 *                 },
 *                 "roles": [
 *                   {
 *                       "data": {
 *                          "type": "role",
 *                          "id": 1,
 *                          "attributes": {
 *                              "title": "Admin",
 *                              "createdAt": "2020-01-14 07:30:08",
 *                              "updatedAt": null
 *                          }
 *                      }
 *                   }
 *                 ]
 *               }
 *          }
 *
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse Authorization
 */

/**
 * @api {put} /api/dashboard/user/password update password
 * @apiVersion 1.0.0
 * @apiName update password
 * @apiGroup Dashboard User Profile
 *
 * @apiParam {String} currentPassword current user password
 * @apiParam {String} password new user password
 * @apiParam {String} passwordConfirmation new user password confirmation
 * @apiSuccess {object} data Data
 * @apiUse dashboardUser
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *          "data": {
 *              "type": "user",
 *               "id": 1,
 *               "attributes": {
 *                   "email": "test@test.com",
 *                   "firstName": "test",
 *                   "lastName": "test",
 *                   "createdAt": "2020-01-18 10:35:07",
 *                   "updatedAt": "2020-01-18 10:35:07"
 *               },
 *               "relationships": {
 *                  "avatar": {
 *                        "data": {
 *                            "type": "file",
 *                            "id": 1,
 *                            "attributes": {
 *                                "storagePath": "http:/site.com/avatar/test/logo.jpg",
 *                                "title": "avatar",
 *                            }
 *                        }
 *                 },
 *                 "roles": [
 *                   {
 *                       "data": {
 *                          "type": "role",
 *                          "id": 1,
 *                          "attributes": {
 *                              "title": "Admin",
 *                              "createdAt": "2020-01-14 07:30:08",
 *                              "updatedAt": null
 *                          }
 *                      }
 *                   }
 *                 ]
 *               }
 *          }
 *      }
 *
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse Authorization
 */

/**
 * @api {post} /api/dashboard/user/avatar upload user avatar
 * @apiVersion 1.0.0
 * @apiName upload user avatar
 * @apiGroup Dashboard User Profile
 *
 * @apiParam {File} avatar user avatar
 *
 * @apiUse CreatedSuccess
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse Authorization
 */

/**
 * @api {delete} /api/dashboard/user/avatar delete user avatar
 * @apiVersion 1.0.0
 * @apiName delete user avatar
 * @apiGroup Dashboard User Profile
 *
 * @apiUse NO_CONTENT
 * @apiUse ValidationError
 * @apiUse INTERNAL_SERVER_ERROR
 * @apiUse UNAUTHORIZED
 * @apiUse FORBIDDEN
 * @apiUse Authorization
 */
