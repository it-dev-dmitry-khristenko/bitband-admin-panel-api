<?php

/**
 * @apiDefine Paginate
 * @apiVersion 1.0.0
 *
 * @apiParam {number} [page] Page number
 * @apiParam {number} [perPage] Limit items on page
 * @apiSuccess {object} meta Meta data
 * @apiSuccess {string} meta.totalPages Total pages
 * @apiSuccess {string} meta.totalItems Total items
 * @apiSuccessExample Success-Response Paginate:
 *     HTTP/1.1 200 OK
 *    {
 *          "data": [...]
 *          "meta": {
 *              "totalPages": 1,
 *              "totalItems": 1
 *          }
 *    }
 */

/**
 * @apiDefine CreatedSuccess
 * @apiVersion 1.0.0
 *
 * @apiSuccess (201) {Object} data Data
 * @apiSuccess (201) {String} data.type Entity type
 * @apiSuccess (201) {Number} [data.id] Entity id
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 Created
 *     {
 *       "data": {
 *              'type': 'user'
 *              'id': 1
 *          }
 *     }
 *
 */

/**
 * @apiDefine Success
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Object} data Data
 * @apiSuccess {String} data.type Entity type
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *              'type': 'user'
 *          }
 *     }
 *
 */

/**
 * @apiDefine Token
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Object} data Data
 * @apiSuccess {String} data.type Entity type
 * @apiSuccess {Object} data.attributes Entity attributes
 * @apiSuccess {string} data.attributes.tokenType Token type
 * @apiSuccess {int} data.attributes.expiresIn Date time when expires token
 * @apiSuccess {string} data.attributes.accessToken Access token
 * @apiSuccess {string} data.attributes.refreshToken Refresh token
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 Ok
 *     {
 *       "data": {
 *              'type': 'token',
 *              'attributes': {
 *                  "tokenType": "Bearer"
 *                  "expiresIn": 31622400
 *                  "accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI..."
 *                  "refreshToken": "def50200b4b645f89a1b2fc5b84..."
 *              }
 *          }
 *     }
 *
 */


/**
 * @apiDefine ValidationError
 * @apiVersion 1.0.0
 *
 * @apiError (422) {Object[]} errors Errors
 * @apiError (422) {String} errors.status error status
 * @apiError (422) {String} errors.detail error message
 * @apiError (422) {Object} [errors.source] Source
 * @apiError (422) {String} errors.source.parameter form parameter
 *
 * @apiErrorExample Error-Response 422:
 *     HTTP/1.1 422 UNPROCESSABLE ENTITY
 *     {
 *       "errors": [
 *          {
 *              "status": 422,
 *              "detail": "Exception message",
 *              "source": {
 *                  "parameter": "fieldName"
 *              }
 *          },
 *          {
 *              "status": 422,
 *              "detail": "Exception message",
 *              "source": {
 *                  "parameter": "fieldName"
 *              }
 *          }
 *        ]
 *     }
 *
 */


/**
 * @apiDefine UNAUTHORIZED
 * @apiVersion 1.0.0
 *
 * @apiError (401) {Object[]} errors Errors
 * @apiError (401) {String} errors.status error status
 * @apiError (401) {String} errors.detail error message
 *
 * @apiErrorExample Error-Response 401:
 *     HTTP/1.1 401 UNAUTHORIZED
 *     {
 *       "errors": [
 *          {
 *              "status": 401,
 *              "detail": "Exception message",
 *          }
 *        ]
 *     }
 *
 */

/**
 * @apiDefine FORBIDDEN
 * @apiVersion 1.0.0
 *
 * @apiError (403) {Object[]} errors Errors
 * @apiError (403) {String} errors.status error status
 * @apiError (403) {String} errors.detail error message
 *
 * @apiErrorExample Error-Response 403:
 *     HTTP/1.1 403 FORBIDDEN
 *     {
 *       "errors": [
 *          {
 *              "status": 403,
 *              "detail": "Exception message",
 *          }
 *        ]
 *     }
 *
 */

/**
 * @apiDefine NO_CONTENT
 * @apiVersion 1.0.0
 *
 * @apiSuccess (204) null
 *
 * @apiSuccessExample Success-Response 204:
 *     HTTP/1.1 204 NO_CONTENT
 *     {
 *     }
 */

/**
 * @apiDefine INTERNAL_SERVER_ERROR
 * @apiVersion 1.0.0
 *
 * @apiError (500) {Object[]} errors Errors
 * @apiError (500) {String} errors.status error status
 * @apiError (500) {String} errors.detail error message
 *
 * @apiErrorExample Error-Response 500:
 *     HTTP/1.1 500 INTERNAL_SERVER_ERROR
 *     {
 *       "errors": [
 *          {
 *              "status": 500,
 *              "detail": "Exception message",
 *          }
 *        ]
 *     }
 *
 */

/**
 * @apiDefine NotFound
 * @apiVersion 1.0.0
 *
 * @apiError (404) {Object[]} errors Errors
 * @apiError (404) {String} errors.status error status
 * @apiError (404) {String} errors.detail error message
 *
 * @apiErrorExample Error-Response 404:
 *     HTTP/1.1 404 NOT FOUND
 *     {
 *       "errors": [
 *          {
 *              "status": 404,
 *              "detail": "Exception message",
 *          }
 *        ]
 *     }
 *
 */

/**
 * @apiDefine Authorization
 * @apiVersion 1.0.0
 * @apiHeader {String} Authorization Bearer token
 * @apiHeaderExample {json} Header-Example:
 * { "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOi..." }
 */
