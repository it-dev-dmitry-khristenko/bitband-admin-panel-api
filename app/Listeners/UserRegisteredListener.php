<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\UserRegisteredEvent;
use App\Mail\UserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserRegisteredListener
 * @package App\Listeners
 */
class UserRegisteredListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  UserRegisteredEvent  $event
     * @return void
     */
    public function handle(UserRegisteredEvent $event)
    {
        $user = $event->getUser();
        Mail::to($user->email)
            ->send(new UserRegistered($user));
    }
}
