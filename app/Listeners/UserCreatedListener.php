<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\UserCreatedEvent;
use App\Mail\UserCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

/**
 * Class UseCreatedListener
 * @package App\Listeners
 */
class UserCreatedListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param UserCreatedEvent $event
     * @return void
     */
    public function handle(UserCreatedEvent $event)
    {
        $user = $event->getUser();
        $password = $event->getPassword();
        Mail::to($user->email)
            ->send(new UserCreated($user, $password));
    }
}
