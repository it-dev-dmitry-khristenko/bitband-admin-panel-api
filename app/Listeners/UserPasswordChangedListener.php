<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\UserPasswordChangedEvent;
use App\Mail\UserPasswordChanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserChangePasswordListener
 * @package App\Listeners
 */
class UserPasswordChangedListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param UserPasswordChangedEvent $event
     * @return void
     */
    public function handle(UserPasswordChangedEvent $event)
    {
        $user = $event->getUser();
        $password = $event->getPassword();
        Mail::to($user->email)
            ->send(new UserPasswordChanged($user, $password));
    }
}
