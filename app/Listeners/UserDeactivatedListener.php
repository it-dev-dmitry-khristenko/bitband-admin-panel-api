<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Domain\AuthToken\AuthTokenFilter;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;
use App\Events\UserDeactivatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class UserDeactivatedListener
 * @package App\Listeners
 */
class UserDeactivatedListener implements ShouldQueue
{
    /**
     * @var AuthTokenRepositoryInterface
     */
    private AuthTokenRepositoryInterface $authTokenRepository;

    /**
     * UserDeactivatedListener constructor.
     * @param AuthTokenRepositoryInterface $authTokenRepository
     */
    public function __construct(
        AuthTokenRepositoryInterface $authTokenRepository
    ) {
        $this->authTokenRepository = $authTokenRepository;
    }

    /**
     * @param UserDeactivatedEvent $event
     */
    public function handle(UserDeactivatedEvent $event)
    {
        $user = $event->getUser();
        $filter = new AuthTokenFilter();
        $filter->setUsers([
            $user->id
        ]);
        $tokens = $this->authTokenRepository->filter($filter)->all();
        foreach ($tokens as $token) {
            $this->authTokenRepository->delete($token);
        }
    }
}
