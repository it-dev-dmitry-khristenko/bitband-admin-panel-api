<?php

declare(strict_types=1);

namespace App\Listeners;

use App\Events\UserResetPasswordRequestEvent;
use App\Mail\UserForgotPasswordMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

/**
 * Class UserForgotPasswordListener
 * @package App\Listeners
 */
class UserForgotPasswordListener implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  UserResetPasswordRequestEvent  $event
     * @return void
     */
    public function handle(UserResetPasswordRequestEvent $event)
    {
        $user = $event->getUser();
        Mail::to($user->email)
            ->send(new UserForgotPasswordMail($user));
    }
}
