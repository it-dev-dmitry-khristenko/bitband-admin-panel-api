<?php

declare(strict_types=1);

namespace App\Mail;

use App\Domain\Role\Role;
use App\Domain\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserCreated
 * @package App\Mail
 */
class UserCreated extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var User
     */
    private User $user;
    /**
     * @var string
     */
    private string $password;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $uiUrl = config('app.ui_url');
        $uiUrl = $uiUrl . (config('app.ui_port') ? ':' . config('app.ui_port') : '');


        if ($this->user->roles->contains(Role::ROLE_ADMIN)) {
            $uiUrl = config('app.ui_dashboard_url');
            $uiUrl = $uiUrl . (config('app.ui_dashboard_port') ? ':' . config('app.ui_dashboard_port') : '');
        }

        return $this->view('mails.user-created', [
            'email' => $this->user->email,
            'password' => $this->password,
            'uiUrl' => $uiUrl,
            'role' => $this->user->roles->contains(Role::ROLE_ADMIN) ? 'user' : 'admin'
        ]);
    }
}
