<?php

declare(strict_types=1);

namespace App\Mail;

use App\Domain\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserRegistered
 * @package App\Mail
 */
class UserRegistered extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var User
     */
    private User $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $uiActivationUrl = config('app.ui_url');
        if (config('app.ui_port')) {
            $uiActivationUrl .= ":" . config('app.ui_port');
        }
        $uiActivationUrl .= "/confirm-email?code=" . $this->user->activation_code;
        return $this->view('mails.user-registered', [
            'uiActivationUrl' => $uiActivationUrl
        ]);
    }
}
