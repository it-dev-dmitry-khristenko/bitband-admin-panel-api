<?php

declare(strict_types=1);

namespace App\Mail;

use App\Domain\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserChangePassword
 * @package App\Mail
 */
class UserPasswordChanged extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var User
     */
    private User $user;
    /**
     * @var string
     */
    private string $password;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.user-password-changed', [
            'email' => $this->user->email,
            'password' => $this->password,
        ]);
    }
}
