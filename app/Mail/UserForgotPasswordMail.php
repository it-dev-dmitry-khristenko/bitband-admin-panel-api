<?php

declare(strict_types=1);

namespace App\Mail;

use App\Domain\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserForgotPasswordMail
 * @package App\Mail
 */
class UserForgotPasswordMail extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * @var User
     */
    private User $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $uiForgotPasswordUrl = config('app.ui_url');
        if (config('app.ui_port')) {
            $uiForgotPasswordUrl .= ":" . config('app.ui_port');
        }
        $uiForgotPasswordUrl .= "/new-password?code=" . $this->user->reset_password_code;
        return $this->view('mails.user-forgot-password', [
            'uiForgotPasswordUrl' => $uiForgotPasswordUrl
        ]);
    }
}
