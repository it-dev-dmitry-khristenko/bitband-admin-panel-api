<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\UserCreatedEvent;
use App\Events\UserDeactivatedEvent;
use App\Events\UserPasswordChangedEvent;
use App\Events\UserRegisteredEvent;
use App\Events\UserResetPasswordRequestEvent;
use App\Listeners\UserCreatedListener;
use App\Listeners\UserDeactivatedListener;
use App\Listeners\UserForgotPasswordListener;
use App\Listeners\UserPasswordChangedListener;
use App\Listeners\UserRegisteredListener;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserRegisteredEvent::class => [
            UserRegisteredListener::class,
        ],
        UserResetPasswordRequestEvent::class => [
            UserForgotPasswordListener::class,
        ],
        UserCreatedEvent::class => [
            UserCreatedListener::class,
        ],
        UserPasswordChangedEvent::class => [
            UserPasswordChangedListener::class,
        ],
        UserDeactivatedEvent::class => [
            UserDeactivatedListener::class
        ]
    ];
}
