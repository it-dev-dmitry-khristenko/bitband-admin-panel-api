<?php

declare(strict_types=1);

namespace App\Providers;

use App\Domain\File\FileRepositoryInterface;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;
use App\Domain\Role\RoleRepositoryInterface;
use App\Domain\User\UserRepositoryInterface;
use App\Infrastructure\DatabaseRepository\FileRepository;
use App\Infrastructure\DatabaseRepository\AuthTokenRepository;
use App\Infrastructure\DatabaseRepository\RoleRepository;
use App\Infrastructure\DatabaseRepository\UserRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class DomainServiceProvider
 * @package App\Providers
 */
class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
        $this->app->singleton(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->singleton(FileRepositoryInterface::class, FileRepository::class);
        $this->app->singleton(AuthTokenRepositoryInterface::class, AuthTokenRepository::class);
    }
}
