<?php

declare(strict_types=1);

namespace App\Providers;

use App\Contract\Core\AppFileServiceInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Domain\Settings\SettingsServiceInterface;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use App\Infrastructure\DatabaseRepository\SettingsRepository;
use App\Infrastructure\Services\AppFileService;
use App\Infrastructure\Services\Settings\SettingsService;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        ##Core
        $this->app->singleton(PaginationInterface::class, Pagination::class);
        $this->app->singleton(SortingInterface::class, Sorting::class);
        $this->app->singleton(AppFileServiceInterface::class, AppFileService::class);
        $this->app->singleton(SettingsRepositoryInterface::class, SettingsRepository::class);
        $this->app->singleton(SettingsServiceInterface::class, SettingsService::class);


        Carbon::serializeUsing(function (Carbon $carbon) {
            return $carbon->format('Y-m-d H:i:s');
        });

        if ($this->app->environment() === 'local') {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }
}
