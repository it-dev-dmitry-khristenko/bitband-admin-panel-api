<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Lumen\Routing\Router;

/**
 * Class RouteServiceProvider
 * @package App\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->router->group([
            'namespace' => 'App\Http\Controllers\Dashboard',
            'prefix' => 'api/dashboard',
            'as' => 'api.dashboard',
            'middleware' => 'locale'
        ], function (Router $router) {
            require __DIR__ . '/../../routes/dashboard.php';
        });
    }
}
