<?php

declare(strict_types=1);

namespace App\Domain\Settings;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class SettingsFilter
 * @package App\Domain\Settings
 */
class SettingsFilter implements FilterInterface
{

    /**
     * @var string|null
     */
    private ?string $group = null;

    /**
     * @var string|null
     */
    private ?string $key = null;

    /**
     * @inheritDoc
     */
    public static function fromRequest(Request $request): FilterInterface
    {
    }

    /**
     * @return string|null
     */
    public function getGroup(): ?string
    {
        return $this->group;
    }

    /**
     * @param string|null $group
     */
    public function setGroup(?string $group): void
    {
        $this->group = $group;
    }

    /**
     * @return string|null
     */
    public function getKey(): ?string
    {
        return $this->key;
    }

    /**
     * @param string|null $key
     */
    public function setKey(?string $key): void
    {
        $this->key = $key;
    }
}
