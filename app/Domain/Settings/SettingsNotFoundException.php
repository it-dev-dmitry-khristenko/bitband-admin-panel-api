<?php

declare(strict_types=1);

namespace App\Domain\Settings;

use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class SettingsNotFound
 * @package App\Domain\Settings
 */
class SettingsNotFoundException extends ModelNotFoundException
{
    /**
     * @param string $key
     * @return static
     */
    public static function keyNotFound(string $key): self
    {
        return new self(__('exception.settings.not_found', [
            'key' => $key
        ]));
    }
}
