<?php

declare(strict_types=1);

namespace App\Domain\Settings;

use Illuminate\Support\Collection;

/**
 * Class AbstractSettingsContainer
 * @package App\Infrastructure\Services\Settings
 */
interface SettingsContainerInterface
{
    /**
     * @return void
     */
    public function init(): void;

    /**
     * @return string
     */
    public function getKey(): string;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return int
     */
    public function getOrder(): int;

    /**
     * @return array
     */
    public function getItems(): array;

    /**
     * @return array
     */
    public function booleans();
}
