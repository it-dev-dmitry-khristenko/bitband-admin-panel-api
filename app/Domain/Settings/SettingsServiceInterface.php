<?php

declare(strict_types=1);

namespace App\Domain\Settings;

use App\Infrastructure\Core\Pagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;

/**
 * Class SettingsService
 * @package App\Domain\Settings
 */
interface SettingsServiceInterface
{
    /**
     * @param string $key
     * @return mixed|null
     * @throws SettingsNotFoundException
     */
    public function getByKey(string $key);

    /**
     * @param string $group
     * @param Pagination $pagination
     * @return LengthAwarePaginator
     * @throws ModelNotFoundException
     */
    public function getByGroup(string $group, Pagination $pagination): LengthAwarePaginator;

    /**
     * @return Collection
     */
    public function getGroupList(): Collection;

    /**
     * @return void
     */
    public function syncSettings(): void;

    /**
     * @param string $key
     * @param string $value
     * @throws ValidationException
     * @throws SettingsNotFoundException
     */
    public function updateValue(string $key, string $value): void;
}
