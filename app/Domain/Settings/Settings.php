<?php

declare(strict_types=1);

namespace App\Domain\Settings;

use App\Domain\Core\BaseModel;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon;

/**
 * Class Settings
 *
 * @package App\Domain
 * @property int $id
 * @property string $group
 * @property string $title
 * @property string|null $description
 * @property string $key
 * @property string|null $value
 * @property bool $is_secret
 * @property string $type
 * @property string $option
 * @property string $validation_rules
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Settings newModelQuery()
 * @method static Builder|Settings newQuery()
 * @method static Builder|Settings query()
 * @method static Builder|Settings whereCreatedAt($value)
 * @method static Builder|Settings whereDescription($value)
 * @method static Builder|Settings whereGroup($value)
 * @method static Builder|Settings whereId($value)
 * @method static Builder|Settings whereKey($value)
 * @method static Builder|Settings whereTitle($value)
 * @method static Builder|Settings whereUpdatedAt($value)
 * @method static Builder|Settings whereValue($value)
 * @method static Builder|Settings whereOption($value)
 * @method static Builder|Settings whereType($value)
 * @method static Builder|Settings whereValidationRules($value)
 * @method static Builder|Settings whereIsSecret($value)
 * @mixin Eloquent
 */
class Settings extends BaseModel
{
    protected $table = 'settings';
}
