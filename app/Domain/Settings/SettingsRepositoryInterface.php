<?php

declare(strict_types=1);

namespace App\Domain\Settings;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Interface SettingRepositoryInterface
 * @package App\Domain\Settings
 */
interface SettingsRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Model|Settings|null
     */
    public function one(): ?Model;

    /**
     * @param PaginationInterface $pagination
     * @return LengthAwarePaginator
     */
    public function paginate(PaginationInterface $pagination): LengthAwarePaginator;

    /**
     * @param FilterInterface $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface;

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository;

    /**
     * @param Model|Settings $setting
     * @return Model|Settings
     */
    public function store(Model $setting): Model;

    /**
     * @param Model|Settings $setting
     */
    public function delete(Model $setting): void;
}
