<?php

declare(strict_types=1);

namespace App\Domain\Core;

use Exception;

/**
 * Class ForbiddenException
 * @package App\Domain\Core
 */
class ForbiddenException extends Exception
{

}
