<?php

declare(strict_types=1);

namespace App\Domain\Core;

use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 *
 * @package App\Domain\User
 * @method static Builder|BaseModel newModelQuery()
 * @method static Builder|BaseModel newQuery()
 * @method static Builder|BaseModel query()
 * @mixin Eloquent
 */
class BaseModel extends Model
{
    /**
     * Fields allowed to sort by
     */
    public const ALLOWED_SORT_FIELDS = [
        'created_at',
        'updated_at'
    ];

    /**
     * Replaces standard created_at field for this model
     *
     * @type string
     */
    public const CREATED_AT = 'created_at';

    /**
     * Replaces standard updated_at field for this model
     *
     * @type string
     */
    public const UPDATED_AT = 'updated_at';

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
}
