<?php

declare(strict_types=1);

namespace App\Domain\File;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class FileFilter
 * @package App\Domain\File
 */
class FileFilter implements FilterInterface
{

    /**
     * @inheritDoc
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        return new self();
    }
}
