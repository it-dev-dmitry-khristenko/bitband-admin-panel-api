<?php

namespace App\Domain\File;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface FileRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Model|File|null
     */
    public function one(): ?Model;

    /**
     * @param PaginationInterface $pagination
     * @return LengthAwarePaginator
     */
    public function paginate(PaginationInterface $pagination): LengthAwarePaginator;

    /**
     * @param FilterInterface $userFilter
     * @return $this
     */
    public function filter(FilterInterface $userFilter): DatabaseRepositoryInterface;

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository;

    /**
     * @param Model|File $file
     * @return Model|File
     */
    public function store(Model $file): Model;

    /**
     * @param Model|File $role
     */
    public function delete(Model $file): void;
}
