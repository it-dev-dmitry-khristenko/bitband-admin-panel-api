<?php

namespace App\Domain\File;

use Exception;

/**
 * Class FileExceptions
 * @package App\Domain\File
 */
class FileExceptions extends Exception
{
    /**
     * @param int $id
     * @throws FileExceptions
     */
    public static function notFound(int $id)
    {
        throw new FileExceptions(sprintf('File with id %d not found', $id));
    }
}
