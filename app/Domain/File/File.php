<?php

declare(strict_types=1);

namespace App\Domain\File;

use App\Domain\Core\BaseModel;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * Class File
 *
 * @package App\Domain\File
 * @property-read Model|Eloquent $owner
 * @method static Builder|File newModelQuery()
 * @method static Builder|File newQuery()
 * @method static Builder|File query()
 * @mixin Eloquent
 * @property int $id
 * @property string $title
 * @property string $storage_path
 * @property string $ext
 * @property string $owner_type
 * @property int $owner_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|File whereCreatedAt($value)
 * @method static Builder|File whereExt($value)
 * @method static Builder|File whereId($value)
 * @method static Builder|File whereOwnerId($value)
 * @method static Builder|File whereOwnerType($value)
 * @method static Builder|File whereStoragePath($value)
 * @method static Builder|File whereTitle($value)
 * @method static Builder|File whereUpdatedAt($value)
 */
class File extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'files';

    /**
     * @var bool
     */
    public $incrementing = true;

    /**
     * @var array
     */
    protected $hidden = [
        'owner_type',
        'owner_id'
    ];

    /**
     * @param string $title
     * @param string $ext
     * @param string $path
     * @param Model $model
     * @return static
     */
    public static function register(string $title, string $ext, string $path, Model $model): self
    {
        $file = new self();
        $file->title = $title;
        $file->ext = $ext;
        $file->storage_path = $path;
        $file->owner_type = get_class($model);
        $file->owner_id = $model->id;

        return $file;
    }

    /**
     * @return MorphTo
     */
    public function owner(): MorphTo
    {
        return $this->morphTo(null, 'owner_type', 'owner_id');
    }
}
