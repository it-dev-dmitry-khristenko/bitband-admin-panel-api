<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface UserRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Model|User|null
     */
    public function one(): ?Model;

    /**
     * @param PaginationInterface $pagination
     * @return LengthAwarePaginator
     */
    public function paginate(PaginationInterface $pagination): LengthAwarePaginator;

    /**
     * @param FilterInterface $filter
     * @return $this
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface;

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository;

    /**
     * @param Model|User $model
     * @return Model|User
     */
    public function store(Model $model): Model;

    /**
     * @param Model|User $model
     */
    public function delete(Model $model): void;
}
