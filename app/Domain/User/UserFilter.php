<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class UserFilter
 * @package App\Domain\User
 */
class UserFilter implements FilterInterface
{
    /** @var integer|null $id */
    private ?int $id = null;

    /** @var string|null $email */
    private ?string $email = null;

    /**
     * @var string|null
     */
    private ?string $searchQuery = null;

    /**
     * @var string|null
     */
    private ?string $activationCode = null;

    /**
     * @var string|null
     */
    private ?string $resetPasswordCode = null;

    /**
     * @var bool|null
     */
    private ?bool $status = null;

    /**
     * @var array|null
     */
    private ?array $ids = null;

    /**
     * @param Request $request
     * @return UserFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();
        $filter->setEmail($request->get('email'));
        $filter->setSearchQuery($request->get('searchQuery'));

        if ($request->exists('status')) {
            $filter->setStatus((bool)$request->get('status'));
        }

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return UserFilter
     */
    public function setId(?int $id): UserFilter
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserFilter
     */
    public function setEmail(?string $email): UserFilter
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSearchQuery(): ?string
    {
        return $this->searchQuery;
    }

    /**
     * @param string|null $searchQuery
     * @return UserFilter
     */
    public function setSearchQuery(?string $searchQuery): UserFilter
    {
        $this->searchQuery = $searchQuery;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getActivationCode(): ?string
    {
        return $this->activationCode;
    }

    /**
     * @param string|null $activationCode
     * @return UserFilter
     */
    public function setActivationCode(?string $activationCode): UserFilter
    {
        $this->activationCode = $activationCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResetPasswordCode(): ?string
    {
        return $this->resetPasswordCode;
    }

    /**
     * @param string|null $resetPasswordCode
     * @return UserFilter
     */
    public function setResetPasswordCode(?string $resetPasswordCode): UserFilter
    {
        $this->resetPasswordCode = $resetPasswordCode;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @param array|null $ids
     * @return UserFilter
     */
    public function setIds(?array $ids): UserFilter
    {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @param bool|null $status
     * @return UserFilter
     */
    public function setStatus(?bool $status): UserFilter
    {
        $this->status = $status;

        return $this;
    }
}
