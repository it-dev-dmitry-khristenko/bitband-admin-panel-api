<?php

declare(strict_types=1);

namespace App\Domain\User;

use Exception;

/**
 * Class UserCantDeleteHimselfException
 * @package App\Domain\User
 */
class UserCantDeleteHimselfException extends Exception
{

}
