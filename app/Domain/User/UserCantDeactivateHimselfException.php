<?php

declare(strict_types=1);

namespace App\Domain\User;

use Exception;

/**
 * Class UserCantDeactivateHimselfException
 * @package App\Domain\User
 */
class UserCantDeactivateHimselfException extends Exception
{

}
