<?php

declare(strict_types=1);

namespace App\Domain\Role;

use Exception;

/**
 * Class RoleExceptions
 * @package App\Domain\Role
 */
class RoleExceptions extends Exception
{
    /**
     * @param int $id
     * @throws RoleExceptions
     */
    public static function notFound(int $id)
    {
        throw new RoleExceptions(sprintf('Role with id %d not found', $id));
    }
}
