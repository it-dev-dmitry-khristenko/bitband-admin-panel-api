<?php

declare(strict_types=1);

namespace App\Domain\Role;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class RoleFilter
 * @package App\Domain\Role
 */
class RoleFilter implements FilterInterface
{
    /** @var integer|null $id */
    private ?int $id = null;

    /**
     * @var array|null
     */
    private ?array $ids = null;

    /**
     * @var string|null
     */
    private ?string $searchQuery = null;

    /**
     * @param Request $request
     * @return RoleFilter
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();
        $filter->setSearchQuery($request->get('searchQuery'));
        $filter->setIds($request->get('roleIds'));

        return $filter;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getSearchQuery(): ?string
    {
        return $this->searchQuery;
    }

    /**
     * @param string|null $searchQuery
     */
    public function setSearchQuery(?string $searchQuery): void
    {
        $this->searchQuery = $searchQuery;
    }

    /**
     * @return array|null
     */
    public function getIds(): ?array
    {
        return $this->ids;
    }

    /**
     * @param array|null $ids
     */
    public function setIds(?array $ids): void
    {
        $this->ids = $ids;
    }
}
