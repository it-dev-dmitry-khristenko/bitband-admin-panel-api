<?php

declare(strict_types=1);

namespace App\Domain\AuthToken;

use Exception;

/**
 * Class UserExceptions
 * @package App\Domain\User
 */
class AuthTokenExceptions extends Exception
{
    /**
     * @throws AuthTokenExceptions
     */
    public static function notFound()
    {
        throw new AuthTokenExceptions('Auth token not found');
    }
}
