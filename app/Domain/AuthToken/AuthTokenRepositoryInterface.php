<?php

declare(strict_types=1);

namespace App\Domain\AuthToken;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Interface RepositoryInterface
 * @package App\Domain
 */
interface AuthTokenRepositoryInterface
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @return Model|AuthToken|null
     */
    public function one(): ?Model;

    /**
     * @param PaginationInterface $pagination
     * @return LengthAwarePaginator
     */
    public function paginate(PaginationInterface $pagination): LengthAwarePaginator;

    /**
     * @param FilterInterface $userFilter
     * @return $this
     */
    public function filter(FilterInterface $userFilter): DatabaseRepositoryInterface;

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository;

    /**
     * @param Model|AuthToken $authToken
     * @return Model|AuthToken
     */
    public function store(Model $authToken): Model;

    /**
     * @param Model|AuthToken $authToken
     */
    public function delete(Model $authToken): void;
}
