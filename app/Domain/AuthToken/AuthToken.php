<?php

declare(strict_types=1);

namespace App\Domain\AuthToken;

use App\Domain\Core\BaseModel;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Carbon as CarbonAlias;

/**
 * Class AuthToken
 *
 * @package App\Domain\AuthToken
 * @property int $id
 * @property int $user_id
 * @property string $token_sign
 * @property CarbonAlias|null $created_at
 * @property CarbonAlias|null $updated_at
 * @method static Builder|AuthToken newModelQuery()
 * @method static Builder|AuthToken newQuery()
 * @method static Builder|AuthToken query()
 * @method static Builder|AuthToken whereCreatedAt($value)
 * @method static Builder|AuthToken whereId($value)
 * @method static Builder|AuthToken whereTokenSign($value)
 * @method static Builder|AuthToken whereUpdatedAt($value)
 * @method static Builder|AuthToken whereUserId($value)
 * @mixin Eloquent
 */
class AuthToken extends BaseModel
{
    /**
     * @var string
     */
    protected $table = 'auth_tokens';
}
