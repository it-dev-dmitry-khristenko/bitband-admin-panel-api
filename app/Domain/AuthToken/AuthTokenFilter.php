<?php

declare(strict_types=1);

namespace App\Domain\AuthToken;

use App\Contract\Core\FilterInterface;
use Illuminate\Http\Request;

/**
 * Class RoleFilter
 * @package App\Domain\Role
 */
class AuthTokenFilter implements FilterInterface
{
    /**
     * @var string
     */
    private ?string $sign = null;

    /**
     * @var array|null
     */
    private ?array $users = null;

    /**
     * @inheritDoc
     */
    public static function fromRequest(Request $request): FilterInterface
    {
        $filter = new self();
        $filter->setSign(explode('.', $request->header('Authorization'))[2]);

        return $filter;
    }

    /**
     * @return string
     */
    public function getSign(): ?string
    {
        return $this->sign;
    }

    /**
     * @param string $sign
     * @return AuthTokenFilter
     */
    public function setSign(?string $sign): AuthTokenFilter
    {
        $this->sign = $sign;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getUsers(): ?array
    {
        return $this->users;
    }

    /**
     * @param array|null $users
     */
    public function setUsers(?array $users): void
    {
        $this->users = $users;
    }
}
