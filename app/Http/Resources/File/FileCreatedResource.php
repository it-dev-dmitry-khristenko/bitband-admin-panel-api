<?php

declare(strict_types=1);

namespace App\Http\Resources\File;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class FileCreatedResourse
 * @package App\Http\Resources\File
 */
class FileCreatedResource extends Resource
{
    /**
     * @param Request $request
     * @return array|void
     */
    public function toArray($request)
    {
        return [
            'type' => 'file',
            'id' => $this->id
        ];
    }
}
