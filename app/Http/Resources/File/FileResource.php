<?php

declare(strict_types=1);

namespace App\Http\Resources\File;

use App\Contract\Core\AppFileServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class FileResource
 * @package App\Http\Resources\File
 */
class FileResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var AppFileServiceInterface $fileService */
        $fileService = app()->make(AppFileServiceInterface::class);
        return [
            'data' => [
                'type' => 'file',
                'id' => $this->id,
                'attributes' => [
                    'storagePath' => $fileService->fullPathByStoragePath($this->storage_path),
                    'title' => $this->title
                ]
            ]
        ];
    }
}
