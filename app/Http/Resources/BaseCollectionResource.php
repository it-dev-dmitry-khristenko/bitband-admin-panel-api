<?php

declare(strict_types=1);

namespace App\Http\Resources;

use App\Infrastructure\Core\Pagination;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class BaseCollectionResource
 * @package App\Http\Resourses
 */
abstract class BaseCollectionResource extends ResourceCollection
{
    /** @var array|null $pagination */
    protected ?array $pagination = null;

    /**
     * @var int
     */
    protected int $totalItems = 0;

    /**
     * @var Request|null
     */
    protected ?Request $request = null;

    /**
     * BaseCollectionResource constructor.
     * @param mixed $collection
     */
    public function __construct($collection)
    {
        if (get_class($collection) == LengthAwarePaginator::class) {
            $this->collection = $collection->items();
            $this->totalItems = $collection->total();
        }
        if (get_class($collection) == Collection::class) {
            $this->collection = $collection;
            $this->totalItems = count($collection);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        // Set request.
        $this->request = $request;

        // Prepare data.
        $response['data'] = [];
        /**
         * @var Model $item
         */
        foreach ($this->collection as $item) {
            $response['data'][] = $this->getItemData($item);
        }

        // Prepare meta data.
        $metaData = $this->getMetaData();
        if (!is_null($metaData)) {
            $response['meta'] = $metaData;
        }

        return $response;
    }

    /**
     * @return array
     */
    protected function getMetaData(): array
    {
        return [
            'totalPages' => $this->totalPages(
                $this->totalItems,
                Pagination::getPerPageFromRequest($this->request)
            ),
            'totalItems' => $this->totalItems
        ];
    }

    /**
     * @param Model $item
     * @return Resource
     */
    abstract protected function getItemData($item): Resource;

    /**
     * @return array|null
     */
    protected function getPagination(): ?array
    {
        return $this->pagination;
    }

    /**
     * @param int $perPage
     * @param int $totalItems
     * @return int
     */
    protected function totalPages($totalItems, $perPage): int
    {
        if (!$perPage) {
            return 1;
        }
        return (int)ceil($totalItems / $perPage);
    }
}
