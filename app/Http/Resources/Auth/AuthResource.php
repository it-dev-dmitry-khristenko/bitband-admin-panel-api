<?php

declare(strict_types=1);

namespace App\Http\Resources\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class AuthResource
 * @package App\Http\Resources\Auth
 */
class AuthResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'token',
            'attributes' => [
                'tokenType' => $this->resource['tokenType'],
                'expiresIn' => $this->resource['expiresIn'],
                'accessToken' => $this->resource['accessToken'],
            ],
        ];
    }
}
