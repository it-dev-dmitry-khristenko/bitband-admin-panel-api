<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserForgotPasswordResource
 * @package App\Http\Resourses\User\Auth
 */
class UserForgotPasswordResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'type' => 'user',
        ];
    }
}
