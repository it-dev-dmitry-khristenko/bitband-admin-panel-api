<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\User;

use App\Domain\User\User;
use App\Http\Resources\Dashboard\Role\ListRoleResource;
use App\Http\Resources\File\FileResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UserResource
 * @package App\Http\Resourses\User
 */
class UserResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $roles = [];
        /**
         * @var User $user
         */
        $user = $this->resource;
        foreach ($user->roles as $role) {
            $roles['data'][] =  new ListRoleResource($role);
        }

        return [
            'id' => $user->id,
            'type' => 'user',
            'attributes' => [
                'email' => $user->email,
                'firstName' => $user->first_name,
                'lastName' => $user->last_name,
                'isActive' => $user->is_active,
                'createdAt' => $user->created_at,
                'updatedAt' => $user->updated_at,
            ],
            'relationships' => [
                'roles' => $roles,
                'avatar' => new FileResource($user->avatar),
            ]
        ];
    }
}
