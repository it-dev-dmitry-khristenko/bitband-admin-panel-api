<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\User;

use App\Http\Resources\BaseCollectionResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class UsersListResource
 * @package App\Http\Resourses\User
 */
class UserResourceCollection extends BaseCollectionResource
{
    /**
     * @param Model $item
     * @return Resource
     */
    protected function getItemData($item): Resource
    {
        return new ListUserResource($item);
    }
}
