<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\Role;

use App\Http\Resources\BaseCollectionResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class RolesListResource
 * @package App\Http\Resourses\Role
 */
class RoleResourceCollection extends BaseCollectionResource
{
    /**
     * @param Model $item
     * @return Resource
     */
    protected function getItemData($item): Resource
    {
        return new ListRoleResource($item);
    }
}
