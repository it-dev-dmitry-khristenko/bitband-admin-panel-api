<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\Role;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class RoleResource
 * @package App\Http\Resourses\Role
 */
class RoleResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'type' => 'role',
            'attributes' => [
                'title' => $this->title,
                'createdAt' => $this->created_at,
                'updatedAt' => $this->updated_at
            ]
        ];
    }
}
