<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\Settings;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class SettingsGroupResource
 * @package App\Http\Resources\Dashboard\Settings
 */
class SettingsGroupResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'settingsGroup',
            'attributes' => [
                'title' => $this->getTitle(),
                'key' => $this->getKey(),
                'order' => $this->getOrder(),
            ]
        ];
    }
}
