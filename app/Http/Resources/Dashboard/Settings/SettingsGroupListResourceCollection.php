<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\Settings;

use App\Http\Resources\BaseCollectionResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class SettingsGroupListResourceCollection
 * @package App\Http\Resources\Dashboard\Settings
 */
class SettingsGroupListResourceCollection extends BaseCollectionResource
{
    /**
     * @inheritDoc
     */
    protected function getItemData($item): Resource
    {
        return new SettingsGroupResource($item);
    }
}
