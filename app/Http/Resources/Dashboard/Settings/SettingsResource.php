<?php

declare(strict_types=1);

namespace App\Http\Resources\Dashboard\Settings;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Class SettingsResource
 * @package App\Http\Resources\Dashboard\Settings
 */
class SettingsResource extends Resource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'settings',
            'id' => $this->id,
            'attributes' => [
                'key' => $this->key,
                'value' => !$this->is_secret ? $this->value : '******',
                'group' => $this->group,
                'title' => $this->title,
                'description' => $this->description,
                'type' => $this->type,
                'option' => $this->option,
                'isSecret' => $this->is_secret,
                'validationRules' => $this->validation_rules,
            ]
        ];
    }
}
