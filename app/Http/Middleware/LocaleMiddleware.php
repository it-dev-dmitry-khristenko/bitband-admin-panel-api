<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Lumen\Http\Request;

/**
 * Class LocaleMiddleware
 * @package App\Http\Middleware
 */
class LocaleMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws UnauthorizedException
     */
    public function handle(Request $request, Closure $next)
    {
        $defaultLocaleCode = config('app.locale');
        app()->setLocale($defaultLocaleCode);
        return $next($request);
    }
}
