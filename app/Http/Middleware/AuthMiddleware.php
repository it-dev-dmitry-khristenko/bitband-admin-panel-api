<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Domain\AuthToken\AuthTokenFilter;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;
use App\Domain\User\User;
use Closure;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Lumen\Http\Request;

/**
 * Class AuthMiddleware
 * @package App\Http\Middleware
 */
class AuthMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws UnauthorizedException
     */
    public function handle(Request $request, Closure $next)
    {
        /**
         * @var User $user
         */
        $user = $request->user();

        /** @var AuthTokenRepositoryInterface $authTokenRepository */
        $authTokenRepository = app()->make(AuthTokenRepositoryInterface::class);

        if (!$user || !$request->header('Authorization')) {
            throw new UnauthorizedException('Not authorized');
        }

        $authToken = $authTokenRepository->filter(AuthTokenFilter::fromRequest($request))
            ->one();

        if (!$authToken || $user->id !== $authToken->user_id) {
            throw new UnauthorizedException('Not authorized');
        }

        $role = collect(array_slice(func_get_args(), 2))->first();
        if (!$user->roles->contains($role)) {
            throw new UnauthorizedException('Not authorized');
        }

        return $next($request);
    }
}
