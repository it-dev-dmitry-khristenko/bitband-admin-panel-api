<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Auth;

use App\Http\Requests\FormRequest;

/**
 * Class RestorePasswordRequest
 * @package App\Http\Requests\User\Auth
 */
class RestorePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|exists:users,reset_password_code',
            'password' => 'required|string|min:6|max:15|same:passwordConfirmation',
            'passwordConfirmation' => 'required|string|min:6|max:15|same:password',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
