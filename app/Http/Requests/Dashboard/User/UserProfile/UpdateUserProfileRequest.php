<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\UserProfile;

use App\Http\Requests\FormRequest;

/**
 * Class UpdateUserProfileRequest
 * @package App\Http\Requests\User\UserProfile
 */
class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'nullable|string|min:2|max:50',
            'lastName' => 'nullable|string|min:2|max:50',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
