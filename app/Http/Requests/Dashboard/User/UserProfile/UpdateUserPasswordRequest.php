<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\UserProfile;

use App\Http\Requests\FormRequest;

/**
 * Class UpdateUserPasswordRequest
 * @package App\Http\Requests\User\UserProfile
 */
class UpdateUserPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currentPassword' => 'required|string|min:6|max:15',
            'password' => 'required|string|min:6|max:15',
            'passwordConfirmation' => 'required|string|min:6|max:15|same:password',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
