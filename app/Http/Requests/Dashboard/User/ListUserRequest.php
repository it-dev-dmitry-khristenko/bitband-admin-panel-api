<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\User;

use App\Http\Requests\FormRequest;

/**
 * Class ListUserRequest
 * @package App\Http\Requests\Dashboard\User
 */
class ListUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'page' => 'nullable|array',
            'page.number' => 'nullable|numeric',
            'page.size' => 'nullable|numeric',
            'status' => 'nullable|numeric'
        ];
    }
}
