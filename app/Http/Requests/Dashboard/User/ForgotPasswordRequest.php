<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Auth;

use App\Http\Requests\FormRequest;

/**
 * Class ForgotPasswordRequest
 * @package App\Http\Requests\User\Auth
 */
class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:5|max:100|exists:users,email',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
