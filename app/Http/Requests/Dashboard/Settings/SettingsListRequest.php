<?php

declare(strict_types=1);

namespace App\Http\Requests\Dashboard\Settings;

use App\Http\Requests\FormRequest;

/**
 * Class SettingsListRequest
 * @package App\Http\Requests\Dashboard\Settings
 */
class SettingsListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'nullable|array',
            'page.number' => 'nullable|numeric',
            'page.size' => 'nullable|numeric',
        ];
    }
}
