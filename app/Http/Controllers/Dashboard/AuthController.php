<?php

declare(strict_types=1);

namespace App\Http\Controllers\Dashboard;

use App\Application\Auth\LoginIntoDashboard\LoginIntoDashboard;
use App\Application\Auth\RefreshToken\RefreshToken;
use App\Application\Logout\Logout;
use App\Application\User\ForgotPassword\ForgotPassword;
use App\Application\User\LoginUser\LoginUser;
use App\Application\User\RestoreUserPassword\RestoreUserPassword;
use App\Domain\AuthToken\AuthTokenFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Auth\LoginRequest;
use App\Http\Requests\Dashboard\Auth\ForgotPasswordRequest;
use App\Http\Requests\Dashboard\Auth\RestorePasswordRequest;
use App\Http\Resources\Auth\AuthResource;
use App\Http\Resources\Dashboard\Auth\UserForgotPasswordResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class AuthController
 * @package App\Http\Controllers\Dashboard
 */
class AuthController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $tokenData = $this->execute(new LoginIntoDashboard($request->all()));
        return new JsonResponse(
            [
                'data' => new AuthResource($tokenData)
            ]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function refreshToken(Request $request): JsonResponse
    {
        $tokenData = $this->execute(new RefreshToken($request->header('Authorization')));
        return new JsonResponse(
            [
                'data' => new AuthResource($tokenData)
            ]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        /** @var AuthTokenFilter $filter */

        $filter = AuthTokenFilter::fromRequest($request);
        $this->execute(new Logout($filter));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param ForgotPasswordRequest $request
     * @return JsonResponse
     */
    public function forgotPassword(ForgotPasswordRequest $request): JsonResponse
    {
        $user = $this->execute(
            new ForgotPassword(
                $request->all()
            )
        );
        return new JsonResponse(
            [
                'data' => new UserForgotPasswordResource($user)
            ]
        );
    }

    /**
     * @param RestorePasswordRequest $request
     * @return JsonResponse
     */
    public function restorePassword(RestorePasswordRequest $request): JsonResponse
    {
        $user = $this->execute(
            new RestoreUserPassword(
                $request->get('code'),
                $request->get('password')
            )
        );

        $tokenData = $this->execute(new LoginUser($user));

        return new JsonResponse(
            [
                'data' => new AuthResource($tokenData)
            ]
        );
    }
}
