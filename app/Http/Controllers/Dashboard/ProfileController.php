<?php

declare(strict_types=1);

namespace App\Http\Controllers\Dashboard;

use App\Application\User\DeleteUserAvatar\DeleteUserAvatar;
use App\Application\User\UpdateUserProfile\UpdateUserPassword;
use App\Application\User\UpdateUserProfile\UpdateUserProfile;
use App\Application\User\UploadUserAvatar\UploadUserAvatar;
use App\Domain\File\File;
use App\Domain\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\UserProfile\UpdateUserPasswordRequest;
use App\Http\Requests\Dashboard\UserProfile\UpdateUserProfileRequest;
use App\Http\Requests\Dashboard\UserProfile\UploadUserAvatarRequest;
use App\Http\Resources\Dashboard\User\UserResource;
use App\Http\Resources\File\FileCreatedResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProfileController
 * @package App\Http\Controllers\Dashboard
 */
class ProfileController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function me(Request $request): JsonResponse
    {
        $user = $request->user();

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ]
        );
    }

    /**
     * @param UpdateUserProfileRequest $request
     * @return JsonResponse
     */
    public function updateProfile(UpdateUserProfileRequest $request): JsonResponse
    {
        $user = $request->user();
        $user = $this->execute(
            new UpdateUserProfile(
                $user,
                $request->all()
            )
        );

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ]
        );
    }

    /**
     * @param UpdateUserPasswordRequest $request
     * @return JsonResponse
     */
    public function updatePassword(UpdateUserPasswordRequest $request): JsonResponse
    {
        $user = $request->user();
        $user = $this->execute(
            new UpdateUserPassword(
                $user,
                $request->all()
            )
        );

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ]
        );
    }

    /**
     * @param UploadUserAvatarRequest $request
     * @return JsonResponse
     */
    public function uploadAvatar(UploadUserAvatarRequest $request): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();

        /** @var File $file */
        $file = $this->execute(
            new UploadUserAvatar(
                $user,
                $request->file('avatar')
            )
        );

        return new JsonResponse(
            [
                'data' => new FileCreatedResource($file)
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteAvatar(Request $request): JsonResponse
    {
        $user = $request->user();

        $this->execute(new DeleteUserAvatar($user));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
