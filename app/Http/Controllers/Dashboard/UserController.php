<?php

declare(strict_types=1);

namespace App\Http\Controllers\Dashboard;

use App\Application\User\ChangePassword\ChangePassword;
use App\Application\User\DeleteUser\DeleteUser;
use App\Application\User\GetUserByFilter\GetUserByFilter;
use App\Application\User\GetUsersList\GetUsersList;
use App\Application\User\StoreUser\StoreUser;
use App\Application\User\UpdateUser\UpdateUser;
use App\Domain\User\User;
use App\Domain\User\UserCantDeactivateHimselfException;
use App\Domain\User\UserCantDeleteHimselfException;
use App\Domain\User\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\User\ChangePasswordRequest;
use App\Http\Requests\Dashboard\User\CreateRequest;
use App\Http\Requests\Dashboard\User\ListUserRequest;
use App\Http\Requests\Dashboard\User\UpdateRequest;
use App\Http\Resources\Dashboard\User\UserResource;
use App\Http\Resources\Dashboard\User\UserResourceCollection;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class UserController
 * @package App\Http\Controllers\Dashboard
 */
class UserController extends Controller
{
    /**
     * @param ListUserRequest $request
     * @return JsonResponse
     */
    public function index(ListUserRequest $request): JsonResponse
    {
        $list = $this->execute(new GetUsersList(
            UserFilter::fromRequest($request),
            Pagination::fromRequest($request),
            Sorting::fromRequest($request, User::ALLOWED_SORT_FIELDS)
        ));

        return new JsonResponse(
            new UserResourceCollection($list),
            Response::HTTP_OK
        );
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId($id);

        $user = $this->execute(new GetUserByFilter($filter));

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @param CreateRequest $request
     * @return JsonResponse
     */
    public function store(CreateRequest $request): JsonResponse
    {
        $user = $this->execute(new StoreUser(
            $request->get('email'),
            $request->get('roleIds'),
            $request->get('firstName'),
            $request->get('lastName'),
            $request->get('isActive')
        ));

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ],
            Response::HTTP_CREATED
        );
    }

    /**
     * @param UpdateRequest $request
     * @param integer $id
     * @return JsonResponse
     * @throws UserCantDeactivateHimselfException
     */
    public function update(UpdateRequest $request, int $id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId($id);

        $user = $this->execute(new GetUserByFilter($filter));

        if (
            $user->id == $request->user()->id
            && $user->is_active
            && is_bool($request->get('isActive'))
            && !$request->get('isActive')
        ) {
            throw new UserCantDeactivateHimselfException(
                __('auth.user_cant_deactivate_himself')
            );
        }

        /**
         * @var User $user
         */
        $user = $this->execute(new UpdateUser(
            $user,
            $request->get('email'),
            $request->get('firstName'),
            $request->get('lastName'),
            $request->get('isActive')
        ));

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ],
            Response::HTTP_OK
        );
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws UserCantDeleteHimselfException
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        $filter = new UserFilter();
        $filter->setId($id);

        $user = $this->execute(new GetUserByFilter($filter));

        if ($user->id == $request->user()->id) {
            throw new UserCantDeleteHimselfException(
                __('auth.user_cant_delete_himself')
            );
        }

        $this->execute(new DeleteUser($user, $request->user()));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param ChangePasswordRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function changePassword(ChangePasswordRequest $request, int $id): JsonResponse
    {
        $user = $this->execute(new ChangePassword(
            $id,
            $request->get('password')
        ));

        return new JsonResponse(
            [
                'data' => new UserResource($user)
            ],
            Response::HTTP_OK
        );
    }
}
