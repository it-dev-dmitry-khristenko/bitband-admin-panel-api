<?php

declare(strict_types=1);

namespace App\Http\Controllers\Dashboard;

use App\Application\Role\GetRolesList\GetRolesList;
use App\Domain\Role\Role;
use App\Domain\Role\RoleFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Role\ListRoleRequest;
use App\Http\Resources\Dashboard\Role\RoleResourceCollection;
use App\Infrastructure\Core\Pagination;
use App\Infrastructure\Core\Sorting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class RoleController
 * @package App\Http\Controllers\Dashboard
 */
class RoleController extends Controller
{
    /**
     * @param ListRoleRequest $request
     * @return JsonResponse
     */
    public function index(ListRoleRequest $request): JsonResponse
    {
        $list = $this->execute(new GetRolesList(
            RoleFilter::fromRequest($request),
            Pagination::fromRequest($request),
            Sorting::fromRequest($request, Role::ALLOWED_SORT_FIELDS)
        ));

        return new JsonResponse(
            new RoleResourceCollection($list),
            Response::HTTP_OK
        );
    }
}
