<?php

declare(strict_types=1);

namespace App\Http\Controllers\Dashboard;

use App\Application\Setting\GetSettingsList\GetSettingsList;
use App\Application\Setting\UpdateSettings\UpdateSettings;
use App\Application\Settings\GetSettingsGroupList\GetSettingsGroupList;
use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\Settings\SettingsListRequest;
use App\Http\Resources\Dashboard\Settings\SettingsGroupListResourceCollection;
use App\Http\Resources\Dashboard\Settings\SettingsResourceCollection;
use App\Infrastructure\Core\Pagination;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class SettingsController
 * @package App\Http\Controllers\Dashboard
 */
class SettingsController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function groups(): JsonResponse
    {
        $groups = $this->execute(new GetSettingsGroupList());

        return new JsonResponse(new SettingsGroupListResourceCollection($groups));
    }

    /**
     * @param SettingsListRequest $request
     * @param string $key
     * @return JsonResponse
     */
    public function settings(SettingsListRequest $request, string $key): JsonResponse
    {
        $settings = $this->execute(new GetSettingsList(
            $key,
            Pagination::fromRequest($request)
        ));

        return new JsonResponse(new SettingsResourceCollection($settings));
    }

    /**
     * @param Request $request
     * @param string $key
     * @return JsonResponse
     */
    public function update(Request $request, string $key): JsonResponse
    {
        $this->execute(new UpdateSettings(
            $key,
            $request->get('value')
        ));

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
