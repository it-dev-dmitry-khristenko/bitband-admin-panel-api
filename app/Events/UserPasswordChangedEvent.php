<?php

declare(strict_types=1);

namespace App\Events;

use App\Domain\User\User;

/**
 * Class UserPasswordChanged
 * @package App\Events
 */
class UserPasswordChangedEvent extends Event
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var string
     */
    private string $password;

    /**
     * UserPasswordChanged constructor.
     * @param User $user
     * @param string $password
     */
    public function __construct(User $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
