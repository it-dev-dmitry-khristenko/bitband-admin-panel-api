<?php

declare(strict_types=1);

namespace App\Events;

use App\Domain\User\User;

/**
 * Class UserResetPasswordRequestEvent
 * @package App\Events
 */
class UserResetPasswordRequestEvent extends Event
{
    /**
     * @var User
     */
    private User $user;

    /**
     * UserResetPasswordRequestEvent constructor.
     * @param User $user
     */
    public function __construct(
        User $user
    ) {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
