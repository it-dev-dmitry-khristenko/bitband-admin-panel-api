<?php

declare(strict_types=1);

namespace App\Events;

use App\Domain\User\User;

/**
 * Class UserActivatedEvent
 * @package App\Events
 */
class UserActivatedEvent extends Event
{
    /**
     * @var User
     */
    private User $user;

    /**
     * UserRegistered constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
