<?php

declare(strict_types=1);

namespace App\Infrastructure\Core;

use App\Contract\Core\PaginationInterface;
use App\Infrastructure\Services\Settings\SettingsManager;
use Illuminate\Http\Request;

/**
 * Class Pagination
 * @package App\Infrastructure\Core
 */
class Pagination implements PaginationInterface
{
    /**
     * Default page and per page values.
     */
    public const DEFAULT_PAGE = 1;

    /** @var int $page */
    private $page;

    /** @var int $perPage */
    private $perPage;

    /**
     * Pagination constructor.
     * @param int $page
     * @param int $perPage
     */
    public function __construct(int $page = 1, int $perPage = 20)
    {
        $this->page = $page;
        $this->perPage = $perPage;
    }

    /**
     * @param Request $request
     * @return PaginationInterface
     */
    public static function fromRequest(Request $request): PaginationInterface
    {
        return new self(
            (int) self::getPageFromRequest($request),
            (int) self::getPerPageFromRequest($request)
        );
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param Request $request
     * @return int
     */
    public static function getPerPageFromRequest(Request $request)
    {
        $page = $request->get('page');
        return $page['size'] ?? SettingsManager::getByKey('main_pagination_per_page');
    }

    /**
     * @param Request $request
     * @return int
     */
    public static function getPageFromRequest(Request $request)
    {
        $page = $request->get('page');
        return $page['number'] ?? self::DEFAULT_PAGE;
    }
}
