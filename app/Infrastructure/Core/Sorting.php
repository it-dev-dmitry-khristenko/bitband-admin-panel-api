<?php

declare(strict_types=1);

namespace App\Infrastructure\Core;

use App\Contract\Core\SortingInterface;
use Illuminate\Http\Request;

/**
 * Class Sorting
 * @package App\Infrastructure\Core
 */
class Sorting implements SortingInterface
{
    /** @var string $field */
    private string $field;

    /** @var string $direction */
    private string $direction;

    /**
     * Sorting constructor.
     * @param string $field
     * @param string $direction
     */
    public function __construct(
        string $field = self::DEFAULT_SORT_FIELD,
        string $direction = self::SORT_DESC
    ) {
        $this->field = $field;
        $this->direction = $direction;
    }

    /**
     * @param Request $request
     * @param array $allowedFields
     * @param string|null $sortParam
     * @param string|null $defaultSortField
     * @return SortingInterface
     */
    public static function fromRequest(
        Request $request,
        array $allowedFields,
        ?string $sortParam = self::DEFAULT_SORT_PARAM,
        ?string $defaultSortField = self::DEFAULT_SORT_FIELD
    ): SortingInterface {
        $sortField = $request->get($sortParam);

        if (!$sortField) {
            return (new self())
                ->setField(self::DEFAULT_SORT_FIELD)
                ->setDirection(self::SORT_DESC);
        }

        $sortDirection = self::SORT_ASC;
        if (substr($sortField, 0, 1) == '-') {
            $sortDirection = self::SORT_DESC;
            $sortField = ltrim($sortField, '-');
        }

        $sortField = in_array($sortField, $allowedFields) ? $sortField : $defaultSortField;

        return (new self())
            ->setField($sortField)
            ->setDirection($sortDirection);
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }

    /**
     * @param string $field
     * @return self
     */
    public function setField($field): SortingInterface
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @param string $direction
     * @return self
     */
    public function setDirection($direction): SortingInterface
    {
        $this->direction = $direction;
        return $this;
    }
}
