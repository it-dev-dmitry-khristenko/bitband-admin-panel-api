<?php

declare(strict_types=1);

namespace App\Infrastructure\Services;

use App\Contract\Core\AppFileServiceInterface;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\Factory;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;

/**
 * Class AppFileService
 * @package App\Infrastructure\Services
 */
class AppFileService implements AppFileServiceInterface
{
    /**
     * @var Factory
     */
    private Factory $storage;

    /**
     * @var string
     */
    private string $driver;

    /**
     * ImageService constructor.
     * @param Factory $storage
     */
    public function __construct(
        Factory $storage
    ) {
        $this->storage = $storage;
        $this->driver = Config::get('filesystems.default');
    }


    /**
     * @param string $path
     * @param string $source
     * @param int $expirationTime
     * @param string|null $driver
     * @return string
     */
    public function saveString(
        string $path,
        string $source,
        int $expirationTime = self::DEFAULT_EXPIRATION_TIME,
        ?string $driver = null
    ): string {
        if (!$driver) {
            $driver = $this->driver;
        }

        $this->storage->disk($driver)->put(
            $path,
            $source
        );

        return $this->fullPathByStoragePath($path, $expirationTime, $driver);
    }

    /**
     * @param string $path
     * @param UploadedFile $source
     * @param int $expirationTime
     * @param string|null $driver
     * @return string
     */
    public function saveUploadFile(
        string $path,
        UploadedFile $source,
        int $expirationTime = self::DEFAULT_EXPIRATION_TIME,
        ?string $driver = null
    ): string {
        if (!$driver) {
            $driver = $this->driver;
        }

        $content = file_get_contents($source->path());

        $this->storage->disk($driver)->put(
            $path,
            $content
        );

        return $this->fullPathByStoragePath($path, $expirationTime, $driver);
    }

    /**
     * @param string $url
     * @param string|null $driver
     */
    public function delete(string $url, ?string $driver = null): void
    {
        if (!$driver) {
            $driver = $this->driver;
        }

        $this->storage->disk($driver)->delete($url);
    }

    /**
     * @param string $storagePath
     * @param int $expirationTime
     * @param string|null $driver
     * @return string
     */
    public function fullPathByStoragePath(
        string $storagePath,
        int $expirationTime = self::DEFAULT_EXPIRATION_TIME,
        ?string $driver = null
    ): string {
        if (!$driver) {
            $driver = $this->driver;
        }

        if ($driver == self::DRIVER_LOCAL || $driver == self::DRIVER_PUBLIC) {
            return url('storage/' . $storagePath);
        }

        return $this->storage->temporaryUrl($storagePath, Carbon::now()->addMinutes($expirationTime));
    }

    /**
     * @param string $path
     * @param string|null $driver
     * @return string
     * @throws FileNotFoundException
     */
    public function getFile(string $path, ?string $driver = null): string
    {
        if (!$driver) {
            $driver = $this->driver;
        }

        return $this->storage->disk($driver)->get($path);
    }

    /**
     * @param string $from
     * @param string $to
     * @param string|null $driver
     */
    public function move(string $from, string $to, ?string $driver = null): void
    {
        if (!$driver) {
            $driver = $this->driver;
        }

        $this->storage->disk($driver)->move($from, $to);
    }
}
