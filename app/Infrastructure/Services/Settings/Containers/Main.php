<?php

declare(strict_types=1);

namespace App\Infrastructure\Services\Settings\Containers;

use App\Infrastructure\Services\Settings\AbstractSettingsContainer;

/**
 * Class Main
 * @package App\Infrastructure\Services\Settings\Modules
 */
class Main extends AbstractSettingsContainer
{
    /**
     * Main constructor.
     */
    public function __construct()
    {
        $this->key = 'main';
        $this->title = __('settings.containers.main');
        $this->order = 1;
    }
}
