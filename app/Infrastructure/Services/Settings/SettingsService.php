<?php

declare(strict_types=1);

namespace App\Infrastructure\Services\Settings;

use App\Domain\Settings\Settings;
use App\Domain\Settings\SettingsContainerInterface;
use App\Domain\Settings\SettingsFilter;
use App\Domain\Settings\SettingsNotFoundException;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Domain\Settings\SettingsServiceInterface;
use App\Infrastructure\Core\Pagination;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Application;
use Throwable;

/**
 * Class SettingsService
 * @package App\Infrastructure\Services\Settings
 */
class SettingsService implements SettingsServiceInterface
{
    /**
     * Define settings container namespace
     * @type string
     */
    private const SETTINGS_CONTAINERS_NAMESPACE = "\App\Infrastructure\Services\Settings\Containers\\";
    /**
     * Define settings container folder
     * @type string
     */
    private const SETTINGS_CONTAINERS_FOLDER = "app/Infrastructure/Services/Settings/Containers";

    /**
     * @var SettingsRepositoryInterface
     */
    private SettingsRepositoryInterface $settingsRepository;

    /**
     * @var array|SettingsContainerInterface[]
     */
    private array $containers = [];
    /**
     * @var Application
     */
    private Application $application;

    /**
     * SettingsService constructor.
     * @param SettingsRepositoryInterface $settingsRepository
     * @param Application $application
     */
    public function __construct(
        SettingsRepositoryInterface $settingsRepository,
        Application $application
    ) {
        $this->settingsRepository = $settingsRepository;
        $this->application = $application;
    }

    /**
     * @inheritDoc
     */
    public function getByKey(string $key)
    {
        $filter = new SettingsFilter();
        $filter->setKey($key);
        try {
            /** @var Settings $item */
            $item = $this->settingsRepository->filter($filter)->one();
        } catch (Throwable $e) {
            return null;
        }

        if (!$item) {
            return null;
        }

        $type = $this->type($item);
        $value = $item->value;
        settype($value, $type);

        return $value;
    }

    /**
     * @inheritDoc
     */
    public function getByGroup(string $group, Pagination $pagination): LengthAwarePaginator
    {
        $this->init();

        if (!isset($this->containers[$group])) {
            throw new ModelNotFoundException();
        }

        $filter = new SettingsFilter();
        $filter->setGroup($group);

        $settings = $this->settingsRepository->filter($filter)->paginate($pagination);
        /** @var Settings $item */
        foreach ($settings->items() as $item) {
            if ($item->option) {
                $method = $item->option;
                $item->option = $this->containers[$group]->$method();
            }
        }

        return $settings;
    }


    /**
     * @inheritDoc
     */
    public function getGroupList(): Collection
    {
        $this->init();

        return Collection::make($this->containers)
            ->sortBy(fn (SettingsContainerInterface $container) => $container->getOrder())
            ->values();
    }

    /**
     * @inheritDoc
     */
    public function syncSettings(): void
    {
        $this->init();

        foreach ($this->containers as $container) {
            $container->init();

            $this->store($container);

            $this->deleteOld($container);
        }
    }

    /**
     * @inheritDoc
     */
    public function updateValue(string $key, string $value): void
    {
        $this->init();

        $filter = new SettingsFilter();
        $filter->setKey($key);
        /** @var Settings $item */
        $item = $this->settingsRepository->filter($filter)->one();

        if (!$item) {
            throw SettingsNotFoundException::keyNotFound($key);
        }

        $this->validate($value, $item->validation_rules);

        $item->value = $value;

        $this->settingsRepository->store($item);
    }

    /**
     * @return void
     */
    private function init(): void
    {
        $files = File::files(base_path(self::SETTINGS_CONTAINERS_FOLDER));
        foreach ($files as $file) {
            $fileName = explode('.', $file->getFilename())[0];
            $className = self::SETTINGS_CONTAINERS_NAMESPACE . $fileName;
            /** @var SettingsContainerInterface $container */
            $container = $this->application->make($className);
            $this->containers[$container->getKey()] = $container;
        }
    }

    /**
     * @param SettingsContainerInterface $container
     */
    private function store(SettingsContainerInterface $container): void
    {
        $filter = new SettingsFilter();
        $filter->setGroup($container->getKey());

        foreach ($container->getItems() as $key => $value) {
            $filter->setKey($key);
            $item = $this->settingsRepository->filter($filter)->one();
            if (!$item) {
                $item = new Settings();
                $item->key = $key;
                $item->value = $value['value'];
                $item->type = $value['type'];
                $item->option = $value['option'] ?? null;
                $item->group = $container->getKey();
            }

            $item->title = $value['title'];
            $item->description = $value['description'] ?? null;
            $item->validation_rules = $value['validationRules'];
            $item->is_secret = $value['isSecret'];
            $this->settingsRepository->store($item);
        }
    }

    /**
     * @param SettingsContainerInterface $container
     */
    private function deleteOld(SettingsContainerInterface $container): void
    {
        $filter = new SettingsFilter();
        $filter->setGroup($container->getKey());
        $dbItems = $this->settingsRepository->filter($filter)->all();


        $fileItems = $container->getItems();
        foreach ($dbItems as $item) {
            if (!isset($fileItems[$item->key])) {
                $this->settingsRepository->delete($item);
            }
        }
    }

    /**
     * @param string $value
     * @param string $rules
     * @throws ValidationException
     */
    private function validate($value, string $rules): void
    {
        $messages = [];
        $validate = $this->application->make('validator')
            ->make(['value' => $value], ['value' => $rules], $messages, []);
        if ($validate->fails()) {
            throw ValidationException::withMessages($validate->errors()->messages());
        }
    }

    /**
     * @param Settings $item
     * @return string
     */
    private function type(Settings $item): string
    {
        if ($item->type == 'number' && is_numeric($item->value) && strpos($item->value, '.') !== false) {
            return 'float';
        }
        if ($item->type == 'number' && is_numeric($item->value)) {
            return 'int';
        }
        return 'string';
    }
}
