<?php

declare(strict_types=1);

namespace App\Infrastructure\Services\Settings;

use App\Domain\Settings\SettingsServiceInterface;

/**
 * Class SettingsManager
 * @package App\Infrastructure\Services\Settings
 */
class SettingsManager
{
    /**
     * @param string $key
     * @return mixed|null
     */
    public static function getByKey(string $key)
    {
        $service = app()->make(SettingsServiceInterface::class);

        return $service->getByKey($key);
    }
}
