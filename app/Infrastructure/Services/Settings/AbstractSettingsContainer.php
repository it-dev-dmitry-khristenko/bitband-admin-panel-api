<?php

declare(strict_types=1);

namespace App\Infrastructure\Services\Settings;

use App\Domain\Settings\SettingsContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class AbstractSettingsContainer
 * @package App\Infrastructure\Services\Settings
 */
abstract class AbstractSettingsContainer implements SettingsContainerInterface
{
    /**
     * @var string
     */
    protected string $key = '';

    /**
     * @var string
     */
    protected string $title = '';

    /**
     * @var int
     */
    protected int $order = 1;

    /**
     * @var array
     */
    private array $items = [];

    /**
     * @return void
     */
    public function init(): void
    {
        $this->items = Yaml::parseFile(resource_path('settings/' . $this->key . '.yml'));
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function booleans()
    {
        return [
            true => __('settings.boolean.yes'),
            false => __('settings.boolean.no')
        ];
    }
}
