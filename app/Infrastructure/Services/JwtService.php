<?php

declare(strict_types=1);

namespace App\Infrastructure\Services;

use App\Contract\Services\JwtServiceInterface;
use App\Domain\AuthToken\AuthToken;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;
use App\Domain\Core\ForbiddenException;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use Carbon\Carbon;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Validation\UnauthorizedException;
use Exception;

/**
 * Class JwtService
 * @package App\Infrastructure\Services
 */
class JwtService implements JwtServiceInterface
{
    /** @var string $key */
    protected string $key = "";

    /** @var int $lifetime (minutes) */
    protected int $lifetime = 60000;

    /** @var UserRepositoryInterface $userRepository */
    protected UserRepositoryInterface $userRepository;
    /**
     * @var AuthTokenRepositoryInterface
     */
    private AuthTokenRepositoryInterface $authTokenRepository;

    /**
     * JwtService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param AuthTokenRepositoryInterface $authTokenRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        AuthTokenRepositoryInterface $authTokenRepository
    ) {
        $this->userRepository = $userRepository;

        $this->key = config('jwt.app_key');
        $this->lifetime = (int)config('jwt.token_lifetime');
        $this->authTokenRepository = $authTokenRepository;
    }

    /**
     * @param User $user
     * @return array
     */
    public function generateTokenToUser(User $user): array
    {
        $now = Carbon::now();

        $tokenData = array(
            "iss" => $user->email,
            "aud" => uniqid(),
            "iat" => $user->name,
            "nbf" => $now->timestamp
        );

        $token = JWT::encode($tokenData, $this->key);

        $sign = explode('.', $token)[2];
        $auhToken = new AuthToken();
        $auhToken->user_id = $user->id;
        $auhToken->token_sign = $sign;

        $this->authTokenRepository->store($auhToken);

        return [
            'tokenType' => 'Bearer',
            'expiresIn' => $now->addHour()->timestamp,
            'accessToken' => $token,
        ];
    }

    /**
     * @param string|null $token
     * @return User|null
     */
    public function authByToken(?string $token): ?User
    {
        $data = $this->getDataByToken($token);

        if ($this->checkExpired($data->nbf)) {
            throw new ExpiredException('Token expired');
        }

        $filter = new UserFilter();
        $filter->setEmail($data->iss);

        return $this->userRepository->filter($filter)->one();
    }

    /**
     * @param string|null $token
     * @return array
     * @throws ForbiddenException
     */
    public function refreshToken(?string $token): array
    {
        $data = $this->getDataByToken($token);

        $filter = new UserFilter();
        $filter->setEmail($data->iss);

        $user = $this->userRepository->filter($filter)->one();

        if (!$user) {
            throw new UnauthorizedException('Token is wrong');
        }

        if (!$user->is_active) {
            throw new ForbiddenException();
        }
        return $this->generateTokenToUser($user);
    }


    /**
     * @param string|null $token
     * @return object
     */
    protected function getDataByToken(?string $token): object
    {
        if (!$token) {
            throw new UnauthorizedException('Token not provided');
        }

        $token = explode(' ', $token)[1];

        try {
            $data = JWT::decode($token, $this->key, ['HS256']);
        } catch (Exception $exception) {
            throw new UnauthorizedException('Token is wrong');
        }

        return $data;
    }

    /**
     * @param $time
     * @return bool
     */
    protected function checkExpired($time): bool
    {
        $diff = Carbon::now()->diffInMinutes(Carbon::parse($time));

        return $diff >= $this->lifetime;
    }
}
