<?php

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\File\File;
use App\Domain\File\FileFilter;
use App\Domain\File\FileRepositoryInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FileRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class FileRepository extends DatabaseRepository implements FileRepositoryInterface
{
    /**
     * FileRepository constructor.
     */
    public function __construct()
    {
        $this->model = new File();
    }

    /**
     * @param FilterInterface|FileFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->getBuilder();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getSearchQuery()) {
            $searchQuery = $filter->getSearchQuery();
            $searchQuery = "%$searchQuery%";
            $this->builder->where(function (Builder $query) use ($searchQuery) {
                $query->orWhere('title', 'ilike', $searchQuery);
            });
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->getBuilder();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
