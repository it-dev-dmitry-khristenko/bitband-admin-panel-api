<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\AuthToken\AuthToken;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;
use App\Domain\AuthToken\AuthTokenFilter;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;

/**
 * Class AuthTokenRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class AuthTokenRepository extends DatabaseRepository implements AuthTokenRepositoryInterface
{
    /**
     * AuthTokenRepository constructor.
     */
    public function __construct()
    {
        $this->model = new AuthToken();
    }

    /**
     * @param FilterInterface|AuthTokenFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->getBuilder();

        if ($filter->getSign()) {
            $this->builder->where('token_sign', $filter->getSign());
        }
        if (is_array($filter->getUsers())) {
            $this->builder->whereIn('user_id', $filter->getUsers());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->getBuilder();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
