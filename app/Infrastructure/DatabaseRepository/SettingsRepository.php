<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Settings\Settings;
use App\Domain\Settings\SettingsFilter;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;

/**
 * Class SettingsRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class SettingsRepository extends DatabaseRepository implements SettingsRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function __construct()
    {
        $this->model = new Settings();
    }

    /**
     * @param FilterInterface|SettingsFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->getBuilder();

        if ($filter->getKey()) {
            $this->builder->where('key', '=', $filter->getKey());
        }

        if ($filter->getGroup()) {
            $this->builder->where('group', '=', $filter->getGroup());
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->getBuilder();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
