<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class UserRepository extends DatabaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * @param FilterInterface|UserFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->getBuilder();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if (is_array($filter->getIds())) {
            $this->builder->whereIn('id', $filter->getIds());
        }

        if ($filter->getEmail()) {
            $this->builder->where('email', 'LIKE', "%{$filter->getEmail()}%");
        }

        if ($filter->getSearchQuery()) {
            $searchQuery = $filter->getSearchQuery();
            $searchQuery = "%$searchQuery%";
            $this->builder->where(function (Builder $query) use ($searchQuery) {
                $query->where('first_name', 'ilike', $searchQuery);
                $query->orWhere('last_name', 'ilike', $searchQuery);
                $query->orWhere('email', 'ilike', $searchQuery);
            });
        }

        if ($filter->getActivationCode()) {
            $this->builder->where('activation_code', '=', $filter->getActivationCode());
        }

        if ($filter->getResetPasswordCode()) {
            $this->builder->where('reset_password_code', '=', $filter->getResetPasswordCode());
        }

        if (is_bool($filter->getStatus())) {
            $this->builder->where('is_active', '=', $filter->getStatus());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->getBuilder();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
