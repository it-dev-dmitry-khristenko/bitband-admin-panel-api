<?php

declare(strict_types=1);

namespace App\Infrastructure\DatabaseRepository;

use App\Contract\Core\DatabaseRepositoryInterface;
use App\Contract\Core\FilterInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Role\Role;
use App\Domain\Role\RoleFilter;
use App\Domain\Role\RoleRepositoryInterface;
use App\Infrasctructure\DatabaseRepository\DatabaseRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class RoleRepository
 * @package App\Infrastructure\DatabaseRepository
 */
class RoleRepository extends DatabaseRepository implements RoleRepositoryInterface
{
    /**
     * RoleRepository constructor.
     */
    public function __construct()
    {
        $this->model = new Role();
    }

    /**
     * @param FilterInterface|RoleFilter $filter
     * @return DatabaseRepositoryInterface
     */
    public function filter(FilterInterface $filter): DatabaseRepositoryInterface
    {
        $this->getBuilder();

        if ($filter->getId()) {
            $this->builder->where('id', '=', $filter->getId());
        }

        if ($filter->getSearchQuery()) {
            $searchQuery = $filter->getSearchQuery();
            $searchQuery = "%$searchQuery%";
            $this->builder->where(function (Builder $query) use ($searchQuery) {
                $query->orWhere('title', 'ilike', $searchQuery);
            });
        }

        if ($filter->getIds()) {
            $this->builder->whereIn('id', $filter->getIds());
        }

        return $this;
    }

    /**
     * @param SortingInterface $sorting
     * @return DatabaseRepository
     */
    public function sorting(SortingInterface $sorting): DatabaseRepository
    {
        $this->getBuilder();

        $this->builder->orderBy($sorting->getField(), $sorting->getDirection());

        return $this;
    }
}
