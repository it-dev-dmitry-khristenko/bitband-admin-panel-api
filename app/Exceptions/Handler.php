<?php

declare(strict_types=1);

namespace App\Exceptions;

use App\Domain\Core\ForbiddenException;
use App\Domain\User\UserCantDeactivateHimselfException;
use App\Domain\User\UserCantDeleteHimselfException;
use Exception;
use Firebase\JWT\ExpiredException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class Handler
 * @package App\Exceptions
 */
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ValidationException::class,
        ForbiddenException::class,
        UnauthorizedException::class,
        UserCantDeleteHimselfException::class,
        UserCantDeactivateHimselfException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response|JsonResponse
     * @throws Exception
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            /** @var ValidationException $exception */
            $errors = [];
            foreach ($exception->errors() as $key => $error) {
                Collection::make($error)->each(
                    function (string $message) use ($key, &$errors) {
                        $errors[] = [
                            'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                            'detail' => $message,
                            'source' => [
                                'parameter' => $key
                            ]
                        ];
                    }
                );
            };
            return (new JsonResponse(['errors' => $errors], Response::HTTP_UNPROCESSABLE_ENTITY));
        }

        if ($exception instanceof NotFoundHttpException || $exception instanceof ModelNotFoundException) {
            /** @var NotFoundHttpException $exception */

            return new JsonResponse(
                [
                    'errors' => [
                        [
                            'status' => Response::HTTP_NOT_FOUND,
                            'detail' => 'Resource not found',
                        ]
                    ]
                ],
                Response::HTTP_NOT_FOUND
            );
        }

        if ($exception instanceof UnauthorizedException) {
            /** @var UnauthorizedException $exception */

            return new JsonResponse(
                [
                    'errors' => [
                        [
                            'detail' => $exception->getMessage() ?: __('auth.unauthenticated'),
                            'status' => Response::HTTP_UNAUTHORIZED
                        ]
                    ]
                ],
                Response::HTTP_UNAUTHORIZED
            );
        }

        if ($exception instanceof ForbiddenException) {
            /** @var ForbiddenException $exception */

            return new JsonResponse(
                [
                    'errors' => [
                        [
                            'detail' => $exception->getMessage() ?: __('auth.forbidden'),
                            'status' => Response::HTTP_FORBIDDEN
                        ]
                    ]
                ],
                Response::HTTP_FORBIDDEN
            );
        }

        if ($exception instanceof ExpiredException) {
            /** @var ExpiredException $exception */

            return new JsonResponse(
                [
                    'errors' => [
                        [
                            'status' => 440,
                            'detail' => $exception->getMessage() ?: 'Unauthorized',
                        ]
                    ]
                ],
                440
            );
        }

        if ($exception instanceof UserCantDeactivateHimselfException
            || $exception instanceof UserCantDeleteHimselfException
        ) {
            return new JsonResponse(
                [
                    'errors' => [
                        [
                            'status' => Response::HTTP_FORBIDDEN,
                            'detail' => $exception->getMessage(),
                        ]
                    ]
                ],
                Response::HTTP_FORBIDDEN
            );
        }

        if ($exception instanceof Exception) {
            /** @var ExpiredException $exception */
            return new JsonResponse(
                [
                    'errors' => [
                        [
                            'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                            'detail' => $exception->getMessage(),
                        ]
                    ]
                ],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return parent::render($request, $exception);
    }
}
