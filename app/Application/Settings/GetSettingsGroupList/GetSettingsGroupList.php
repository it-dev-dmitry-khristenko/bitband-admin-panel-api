<?php

declare(strict_types=1);

namespace App\Application\Settings\GetSettingsGroupList;

use App\Contract\Core\Command;

/**
 * Class GetSettingsGroupList
 * @package App\Application\Settings\GetSettingsGroupList
 */
class GetSettingsGroupList implements Command
{
}
