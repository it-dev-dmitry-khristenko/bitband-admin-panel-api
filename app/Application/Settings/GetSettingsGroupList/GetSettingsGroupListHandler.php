<?php

declare(strict_types=1);

namespace App\Application\Settings\GetSettingsGroupList;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Settings\SettingsServiceInterface;
use Illuminate\Support\Collection;

/**
 * Class GetSettingsGroupListHandler
 * @package App\Application\Settings\GetSettingsGroupList
 */
class GetSettingsGroupListHandler implements Handler
{
    /**
     * @var SettingsServiceInterface
     */
    private SettingsServiceInterface $settingsService;

    /**
     * GetSettingsGroupListHandler constructor.
     * @param SettingsServiceInterface $settingsService
     */
    public function __construct(SettingsServiceInterface $settingsService)
    {
        $this->settingsService = $settingsService;
    }

    /**
     * @param Command|GetSettingsGroupList $command
     * @return mixed|void
     */
    public function handle(Command $command): Collection
    {
        return $this->settingsService->getGroupList();
    }
}
