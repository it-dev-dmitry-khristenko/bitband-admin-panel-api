<?php

declare(strict_types=1);

namespace App\Application\Setting\UpdateSettings;

use App\Contract\Core\Command;

/**
 * Class UpdateSettings
 * @package App\Application\Setting\UpdateSettings
 */
class UpdateSettings implements Command
{
    /**
     * @var string
     */
    private string $key;
    /**
     * @var mixed
     */
    private $value;

    /**
     * UpdateSettings constructor.
     * @param string $key
     * @param $value
     */
    public function __construct(string $key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
