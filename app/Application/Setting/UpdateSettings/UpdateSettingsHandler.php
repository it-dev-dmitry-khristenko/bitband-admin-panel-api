<?php

declare(strict_types=1);

namespace App\Application\Setting\UpdateSettings;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Settings\SettingsNotFoundException;
use App\Domain\Settings\SettingsServiceInterface;
use Illuminate\Validation\ValidationException;

/**
 * Class UpdateSettingsHandler
 * @package App\Application\Setting\UpdateSettings
 */
class UpdateSettingsHandler implements Handler
{
    /**
     * @var SettingsServiceInterface
     */
    private SettingsServiceInterface $settingsService;

    /**
     * UpdateSettingsHandler constructor.
     * @param SettingsServiceInterface $settingsService
     */
    public function __construct(SettingsServiceInterface $settingsService)
    {
        $this->settingsService = $settingsService;
    }

    /**
     * @param Command|UpdateSettings $command
     * @return void
     * @throws ValidationException
     * @throws SettingsNotFoundException
     */
    public function handle(Command $command): void
    {
        $this->settingsService->updateValue($command->getKey(), $command->getValue());
    }
}
