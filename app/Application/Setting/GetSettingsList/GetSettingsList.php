<?php

declare(strict_types=1);

namespace App\Application\Setting\GetSettingsList;

use App\Contract\Core\Command;
use App\Contract\Core\PaginationInterface;

/**
 * Class GetSettingsList
 * @package App\Application\Setting\GetSettingsList
 */
class GetSettingsList implements Command
{
    /**
     * @var string
     */
    private string $group;
    /**
     * @var PaginationInterface
     */
    private PaginationInterface $pagination;

    /**
     * GetSettingsList constructor.
     * @param string $group
     * @param PaginationInterface $pagination
     */
    public function __construct(string $group, PaginationInterface $pagination)
    {
        $this->group = $group;
        $this->pagination = $pagination;
    }

    /**
     * @return string
     */
    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        return $this->pagination;
    }
}
