<?php

declare(strict_types=1);

namespace App\Application\Setting\GetSettingsList;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Settings\SettingsServiceInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class GetSettingsListHandler
 * @package App\Application\Setting\GetSettingsList
 */
class GetSettingsListHandler implements Handler
{
    /**
     * @var SettingsServiceInterface
     */
    private SettingsServiceInterface $settingsService;

    /**
     * GetSettingsListHandler constructor.
     * @param SettingsServiceInterface $settingsService
     */
    public function __construct(SettingsServiceInterface $settingsService)
    {
        $this->settingsService = $settingsService;
    }

    /**
     * @param Command|GetSettingsList $command
     * @return LengthAwarePaginator
     */
    public function handle(Command $command): LengthAwarePaginator
    {
        return $this->settingsService->getByGroup($command->getGroup(), $command->getPagination());
    }
}
