<?php

declare(strict_types=1);

namespace App\Application\Role\GetRoleByFilter;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Role\Role;
use App\Domain\Role\RoleRepositoryInterface;

/**
 * Class GetRoleByFilterHandler
 * @package App\Application\Role\GetRoleByFilter
 */
class GetRoleByFilterHandler implements Handler
{
    /** @var RoleRepositoryInterface $roleRepository */
    private RoleRepositoryInterface $roleRepository;

    /**
     * GetRoleByFilterHandler constructor.
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param GetRoleByFilter|Command $command
     * @return Role
     */
    public function handle(Command $command): Role
    {
        return $this->roleRepository->filter($command->getFilter())
            ->one();
    }
}
