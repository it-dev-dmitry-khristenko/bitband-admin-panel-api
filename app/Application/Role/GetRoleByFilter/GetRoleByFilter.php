<?php

declare(strict_types=1);

namespace App\Application\Role\GetRoleByFilter;

use App\Contract\Core\Command;
use App\Domain\Role\RoleFilter;

/**
 * Class GetRoleByFilter
 * @package App\Application\Role\GetRoleByFilter
 */
class GetRoleByFilter implements Command
{
    /** @var RoleFilter $filter */
    private RoleFilter $filter;

    /**
     * GetRoleByFilter constructor.
     * @param RoleFilter $filter
     */
    public function __construct(RoleFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return RoleFilter
     */
    public function getFilter(): RoleFilter
    {
        return $this->filter;
    }
}
