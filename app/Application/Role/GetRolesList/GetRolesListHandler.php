<?php

declare(strict_types=1);

namespace App\Application\Role\GetRolesList;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Role\RoleRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * Class GetRolesListHandler
 * @package App\Application\Role\GetRolesList
 */
class GetRolesListHandler implements Handler
{
    /** @var RoleRepositoryInterface $userRepository */
    private RoleRepositoryInterface $userRepository;

    /**
     * GetRolesListHandler constructor.
     * @param RoleRepositoryInterface $userRepository
     */
    public function __construct(RoleRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetRolesList|Command $command
     * @return LengthAwarePaginator
     */
    public function handle(Command $command): LengthAwarePaginator
    {
        return $this->userRepository->filter($command->getFilter())
            ->sorting($command->getSorting())
            ->paginate($command->getPagination());
    }
}
