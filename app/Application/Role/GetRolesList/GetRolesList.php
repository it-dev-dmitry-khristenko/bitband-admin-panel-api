<?php

declare(strict_types=1);

namespace App\Application\Role\GetRolesList;

use App\Contract\Core\Command;
use App\Contract\Core\PaginationInterface;
use App\Contract\Core\SortingInterface;
use App\Domain\Role\RoleFilter;

/**
 * Class GetRolesList
 * @package App\Application\Role\GetRolesList
 */
class GetRolesList implements Command
{
    /** @var RoleFilter $filter */
    private RoleFilter $filter;

    /** @var PaginationInterface $pagination */
    private PaginationInterface $pagination;

    /** @var SortingInterface $sorting */
    private SortingInterface $sorting;

    /**
     * GetRolesList constructor.
     * @param RoleFilter $filter
     * @param PaginationInterface $pagination
     * @param SortingInterface $sorting
     */
    public function __construct(
        RoleFilter $filter,
        PaginationInterface $pagination,
        SortingInterface $sorting
    ) {
        $this->filter = $filter;
        $this->pagination = $pagination;
        $this->sorting = $sorting;
    }

    /**
     * @return RoleFilter
     */
    public function getFilter(): RoleFilter
    {
        return $this->filter;
    }

    /**
     * @return PaginationInterface
     */
    public function getPagination(): PaginationInterface
    {
        return $this->pagination;
    }

    /**
     * @return SortingInterface
     */
    public function getSorting(): SortingInterface
    {
        return $this->sorting;
    }
}
