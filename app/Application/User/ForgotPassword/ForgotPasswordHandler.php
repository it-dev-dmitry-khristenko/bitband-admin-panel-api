<?php

declare(strict_types=1);

namespace App\Application\User\ForgotPassword;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use App\Events\UserResetPasswordRequestEvent;
use Carbon\Carbon;
use Exception;
use Illuminate\Validation\ValidationException;

/**
 * Class ForgotPasswordHandler
 *
 * @package App\Application\User\ForgotPassword
 */
class ForgotPasswordHandler implements Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * ForgotPasswordHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ForgotPassword|Command $command
     * @return User
     * @throws Exception
     */
    public function handle(Command $command): User
    {
        $userFilter = new UserFilter();
        $userFilter->setEmail($command->getEmail());
        /**
         * @var User $user
         */
        $user = $this->userRepository->filter($userFilter)
            ->one();

        if (!$user->is_active) {
            throw ValidationException::withMessages([
                "email" => [
                    __('auth.user_inactive')
                ],
            ]);
        }

        $user->reset_password_code = sha1(time() . mt_rand(10000000, 99999999));

        //TODO: make system setting with param how match days reset password code will be alive
        $codeExpiresAt = Carbon::now()->addDays(5);
        $user->reset_password_code_expires_at = $codeExpiresAt;

        $this->userRepository->store($user);

        event(new UserResetPasswordRequestEvent($user));

        return $user;
    }
}
