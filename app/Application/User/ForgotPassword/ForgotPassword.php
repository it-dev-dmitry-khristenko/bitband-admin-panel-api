<?php

declare(strict_types=1);

namespace App\Application\User\ForgotPassword;

use App\Contract\Core\Command;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class ForgotPassword
 *
 * @package App\Application\User\ForgotPassword
 */
class ForgotPassword implements Command
{
    use AutofillTrait;

    /**
     * @var string
     */
    private string $email = "";

    /**
     * ForgotPassword constructor.
     *
     * @param array $formData
     */
    public function __construct(array $formData)
    {
        $this->autofillProperties($formData);
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
