<?php

declare(strict_types=1);

namespace App\Application\User\GetUserByFilter;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class GetUserByFilterHandler
 * @package App\Application\User\GetRoleByFilter
 */
class GetUserByFilterHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /**
     * GetUserByFilterHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUserByFilter|Command $command
     * @return User|null
     */
    public function handle(Command $command): ?User
    {
        return $this->userRepository->filter($command->getFilter())
            ->one();
    }
}
