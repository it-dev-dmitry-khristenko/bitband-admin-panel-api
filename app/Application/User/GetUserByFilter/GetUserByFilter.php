<?php

declare(strict_types=1);

namespace App\Application\User\GetUserByFilter;

use App\Contract\Core\Command;
use App\Domain\User\UserFilter;

/**
 * Class GetRoleByFilter
 * @package App\Application\User\GetRoleByFilter
 */
class GetUserByFilter implements Command
{
    /** @var UserFilter $filter */
    private UserFilter $filter;

    /**
     * GetRoleByFilter constructor.
     * @param UserFilter $filter
     */
    public function __construct(UserFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return UserFilter
     */
    public function getFilter(): UserFilter
    {
        return $this->filter;
    }
}
