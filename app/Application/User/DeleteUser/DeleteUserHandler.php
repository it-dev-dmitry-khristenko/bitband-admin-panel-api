<?php

declare(strict_types=1);

namespace App\Application\User\DeleteUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Core\ForbiddenException;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class DeleteUserHandler
 * @package App\Application\User\DeleteUser
 */
class DeleteUserHandler implements Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * DeleteUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param DeleteUser|Command $command
     * @return void
     * @throws ForbiddenException
     */
    public function handle(Command $command)
    {
        if ($command->getUser()->id === $command->getCurrentUser()->id) {
            throw new ForbiddenException();
        }

        $this->userRepository->delete($command->getUser());
    }
}
