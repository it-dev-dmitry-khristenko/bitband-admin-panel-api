<?php

declare(strict_types=1);

namespace App\Application\User\DeleteUser;

use App\Contract\Core\Command;
use App\Domain\User\User;

/**
 * Class DeleteUser
 * @package App\Application\User\DeleteUser
 */
class DeleteUser implements Command
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var User
     */
    private User $currentUser;

    /**
     * DeleteUser constructor.
     * @param User $user
     * @param User $currentUser
     */
    public function __construct(
        User $user,
        User $currentUser
    ) {
        $this->user = $user;
        $this->currentUser = $currentUser;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return User
     */
    public function getCurrentUser(): User
    {
        return $this->currentUser;
    }
}
