<?php

declare(strict_types=1);

namespace App\Application\User\ChangePassword;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Events\UserPasswordChangedEvent;
use App\Infrastructure\DatabaseRepository\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * Class ChangePasswordHandler
 * @package App\Application\User\ChangePassword
 */
class ChangePasswordHandler implements Handler
{
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * ChangePasswordHandler constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Command|ChangePassword $command
     * @return User
     * @throws ModelNotFoundException
     */
    public function handle(Command $command): User
    {
        $userFilter = new UserFilter();
        $userFilter->setId($command->getUserId());
        /** @var User $user */
        $user = $this->userRepository->filter($userFilter)->one();

        if (!$user) {
            throw (new ModelNotFoundException())->setModel(User::class, $command->getUserId());
        }

        $password = $command->getPassword();

        if (!$password) {
            $password = Str::random(15);
        }
        $user->password = Hash::make($password);
        $this->userRepository->store($user);

        event(new UserPasswordChangedEvent($user, $password));

        return $user;
    }
}
