<?php

declare(strict_types=1);

namespace App\Application\User\ChangePassword;

use App\Contract\Core\Command;

/**
 * Class ChangePassword
 * @package App\Application\User\ChangePassword
 */
class ChangePassword implements Command
{
    /**
     * @var int
     */
    private int $userId;

    /**
     * @var null|string
     */
    private ?string $password;

    /**
     * ChangePassword constructor.
     * @param int $userId
     * @param null|string $password
     */
    public function __construct(int $userId, ?string $password)
    {
        $this->userId = $userId;
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }
}
