<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUserProfile;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;

/**
 * Class UpdateUserProfileHandler
 * @package App\Application\User\UpdateUserProfile
 */
class UpdateUserProfileHandler implements Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * UpdateUserProfileHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateUserProfile|Command $command
     * @return User
     */
    public function handle(Command $command): User
    {
        $user = $command->getUser();
        if ($command->getFirstName()) {
            $user->first_name = $command->getFirstName();
        }
        if ($command->getLastName()) {
            $user->last_name = $command->getLastName();
        }

        $this->userRepository->store($user);

        return $user;
    }
}
