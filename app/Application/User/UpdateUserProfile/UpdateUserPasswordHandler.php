<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUserProfile;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * Class UpdateUserPasswordHandler
 * @package App\Application\User\UpdateUserProfile
 */
class UpdateUserPasswordHandler implements Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * UpdateUserProfileHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateUserPassword|Command $command
     * @return User
     * @throws ValidationException
     */
    public function handle(Command $command): User
    {
        $user = $command->getUser();
        $currentPassswordRaw = $command->getCurrentPassword();

        if (!Hash::check($currentPassswordRaw, $user->password)) {
            throw ValidationException::withMessages([
                "currentPassword" => [
                    __('common.current_password_incorrect')
                ],
            ]);
        }

        if ($command->getPassword()) {
            $user->password = Hash::make($command->getPassword());
        }
        $this->userRepository->store($user);

        return $user;
    }
}
