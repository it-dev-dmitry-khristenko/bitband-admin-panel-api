<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUserProfile;

use App\Contract\Core\Command;
use App\Domain\User\User;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class UpdateUserProfile
 * @package App\Application\User\UpdateUserProfile
 */
class UpdateUserProfile implements Command
{
    use AutofillTrait;

    /**
     * @var User
     */
    private User $user;

    /**
     * @var string|null
     */
    private ?string $firstName = null;

    /**
     * @var string|null
     */
    private ?string $lastName = null;

    /**
     * UpdateUser constructor.
     * @param User $user
     * @param array $formData
     */
    public function __construct(
        User $user,
        array $formData
    ) {
        $this->user = $user;
        $this->autofillProperties($formData);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }
}
