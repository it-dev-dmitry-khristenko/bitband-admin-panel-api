<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUserProfile;

use App\Contract\Core\Command;
use App\Domain\User\User;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class UpdateUserPassword
 * @package App\Application\User\UpdateUserProfile
 */
class UpdateUserPassword implements Command
{
    use AutofillTrait;

    /**
     * @var User
     */
    private User $user;

    /**
     * @var string|null
     */
    private ?string $password = null;

    /**
     * @var string|null
     */
    private ?string $currentPassword = null;

    /**
     * UpdateUser constructor.
     * @param User $user
     * @param array $formData
     */
    public function __construct(
        User $user,
        array $formData
    ) {
        $this->user = $user;
        $this->autofillProperties($formData);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getCurrentPassword(): ?string
    {
        return $this->currentPassword;
    }
}
