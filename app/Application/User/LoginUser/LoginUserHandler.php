<?php

declare(strict_types=1);

namespace App\Application\User\LoginUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Contract\Services\JwtServiceInterface;

/**
 * Class LoginUserHandler
 *
 * @package App\Application\User\LoginUser
 */
class LoginUserHandler implements Handler
{
    /**
     * @var JwtServiceInterface
     */
    private JwtServiceInterface $jwtService;

    /**
     * LoginUserHandler constructor.
     * @param JwtServiceInterface $jwtService
     */
    public function __construct(
        JwtServiceInterface $jwtService
    ) {
        $this->jwtService = $jwtService;
    }
    /**
     * @param LoginUser|Command $command
     * @return array
     */
    public function handle(Command $command): array
    {
        $user = $command->getUser();
        return $this->jwtService->generateTokenToUser($user);
    }
}
