<?php

declare(strict_types=1);

namespace App\Application\User\LoginUser;

use App\Contract\Core\Command;
use App\Domain\User\User;
use App\Infrastructure\Traits\AutofillTrait;

/**
 * Class LoginUser
 *
 * @package App\Application\User\LoginUser
 */
class LoginUser implements Command
{
    /**
     * @var User
     */
    private User $user;

    /**
     * LoginUser constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
