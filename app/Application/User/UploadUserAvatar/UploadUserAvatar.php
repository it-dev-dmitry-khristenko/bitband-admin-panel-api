<?php

declare(strict_types=1);

namespace App\Application\User\UploadUserAvatar;

use App\Contract\Core\Command;
use App\Domain\User\User;
use Illuminate\Http\UploadedFile;

/**
 * Class UploadUserAvatar
 * @package App\Application\User\UploadUserAvatar
 */
class UploadUserAvatar implements Command
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var UploadedFile
     */
    private UploadedFile $avatar;

    /**
     * UploadUserAvatar constructor.
     * @param User $user
     * @param UploadedFile $avatar
     */
    public function __construct(User $user, UploadedFile $avatar)
    {
        $this->user = $user;
        $this->avatar = $avatar;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return UploadedFile
     */
    public function getAvatar(): UploadedFile
    {
        return $this->avatar;
    }
}
