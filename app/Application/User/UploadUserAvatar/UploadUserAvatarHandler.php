<?php

declare(strict_types=1);

namespace App\Application\User\UploadUserAvatar;

use App\Contract\Core\AppFileServiceInterface;
use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\File\File;
use App\Domain\File\FileRepositoryInterface;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use League\Flysystem\FileExistsException;

/**
 * Class UploadUserAvatarHandler
 * @package App\Application\User\UploadUserAvatar
 */
class UploadUserAvatarHandler implements Handler
{
    /**
     * @var AppFileServiceInterface
     */
    private AppFileServiceInterface $fileService;
    /**
     * @var FileRepositoryInterface
     */
    private FileRepositoryInterface $fileRepository;

    /**
     * UploadUserAvatarHandler constructor.
     * @param AppFileServiceInterface $fileService
     * @param FileRepositoryInterface $fileRepository
     */
    public function __construct(
        AppFileServiceInterface $fileService,
        FileRepositoryInterface $fileRepository
    ) {
        $this->fileService = $fileService;
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param Command|UploadUserAvatar $command
     * @return File
     */
    public function handle(Command $command): File
    {
        $user = $command->getUser();
        $fileUpload = $command->getAvatar();

        if ($oldAvatar = $user->avatar) {
            try {
                $this->fileService->delete($oldAvatar->storagePath);
            } catch (FileExistsException $e) {
                Log::error($e->getMessage(), $e->getTrace());
            }
            $this->fileRepository->delete($oldAvatar);
        }

        $title = Str::random();
        $ext = $fileUpload->guessClientExtension();

        $file = File::register(
            $title,
            $ext,
            "avatar/{$user->email}/{$title}.{$ext}",
            $user
        );

        $this->fileService->saveUploadFile($file->storage_path, $fileUpload);

        $this->fileRepository->store($file);

        return $file;
    }
}
