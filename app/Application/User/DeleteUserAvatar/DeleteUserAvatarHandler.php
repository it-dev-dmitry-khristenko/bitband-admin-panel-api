<?php

declare(strict_types=1);

namespace App\Application\User\DeleteUserAvatar;

use App\Contract\Core\AppFileServiceInterface;
use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\File\FileRepositoryInterface;

/**
 * Class DeleteUserAvatarHandler
 * @package App\Application\User\DeleteUserAvatar
 */
class DeleteUserAvatarHandler implements Handler
{
    /**
     * @var FileRepositoryInterface
     */
    private FileRepositoryInterface $fileRepository;
    /**
     * @var AppFileServiceInterface
     */
    private AppFileServiceInterface $fileService;

    /**
     * DeleteUserAvatarHandler constructor.
     * @param FileRepositoryInterface $fileRepository
     * @param AppFileServiceInterface $fileService
     */
    public function __construct(
        FileRepositoryInterface $fileRepository,
        AppFileServiceInterface $fileService
    ) {
        $this->fileRepository = $fileRepository;
        $this->fileService = $fileService;
    }

    /**
     * @param Command|DeleteUserAvatar $command
     * @return void
     */
    public function handle(Command $command): void
    {
        $user = $command->getUser();

        if ($avatar = $user->avatar) {
            $this->fileService->delete($avatar->storage_path);
            $this->fileRepository->delete($avatar);
        }
    }
}
