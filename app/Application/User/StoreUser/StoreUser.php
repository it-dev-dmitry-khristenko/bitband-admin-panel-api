<?php

declare(strict_types=1);

namespace App\Application\User\StoreUser;

use App\Contract\Core\Command;

/**
 * Class StoreUser
 * @package App\Application\User\StoreUser
 */
class StoreUser implements Command
{
    /**
     * @var string
     */
    private string $email = "";
    /**
     * @var string|null
     */
    private ?string $firstName = null;
    /**
     * @var string|null
     */
    private ?string $lastName = null;
    /**
     * @var array
     */
    private array $roleIds;
    /**
     * @var bool|null
     */
    private ?bool $isActive;

    /**
     * StoreUser constructor.
     * @param string $email
     * @param array $roleIds
     * @param string $firstName
     * @param string $lastName
     * @param bool|null $isActive
     */
    public function __construct(
        string $email,
        array $roleIds,
        ?string $firstName,
        ?string $lastName,
        ?bool $isActive
    ) {
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->roleIds = $roleIds;
        $this->isActive = $isActive;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return array
     */
    public function getRoleIds(): array
    {
        return $this->roleIds;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }
}
