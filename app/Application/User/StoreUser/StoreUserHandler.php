<?php

declare(strict_types=1);

namespace App\Application\User\StoreUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\Role\RoleFilter;
use App\Domain\Role\RoleRepositoryInterface;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use App\Events\UserCreatedEvent;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * Class StoreUserHandler
 * @package App\Application\User\StoreUser
 */
class StoreUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;
    /**
     * @var RoleRepositoryInterface
     */
    private RoleRepositoryInterface $roleRepository;

    /**
     * StoreUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        RoleRepositoryInterface $roleRepository
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param Command|StoreUser $command
     * @return User
     */
    public function handle(Command $command): User
    {
        $user = new User([
            'email' => $command->getEmail(),
            'first_name' => $command->getFirstName(),
            'last_name' => $command->getLastName(),
        ]);

        $password = Str::random();
        $user->password = Hash::make($password);

        if (is_bool($command->getIsActive())) {
            $user->is_active = $command->getIsActive();
        }

        $this->userRepository->store($user);

        $filter = new RoleFilter();
        $filter->setIds($command->getRoleIds());
        $role = $this->roleRepository->filter($filter)->one();

        $user->roles()->saveMany([$role]);

        event(new UserCreatedEvent($user, $password));

        return $user;
    }
}
