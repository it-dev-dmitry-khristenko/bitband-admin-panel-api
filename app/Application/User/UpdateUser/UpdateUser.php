<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUser;

use App\Contract\Core\Command;
use App\Domain\User\User;

/**
 * Class UpdateUser
 * @package App\Application\User\UpdateUser
 */
class UpdateUser implements Command
{
    /**
     * @var User
     */
    private User $user;
    /**
     * @var string
     */
    private string $email;
    /**
     * @var string|null
     */
    private ?string $firstName = null;
    /**
     * @var string|null
     */
    private ?string $lastName = null;

    /**
     * @var bool|null
     */
    private ?bool $isActive;


    /**
     * UpdateUser constructor.
     * @param User $user
     * @param string $email
     * @param string|null $firstName
     * @param string|null $lastName
     * @param bool|null $isActive
     */
    public function __construct(
        User $user,
        string $email,
        ?string $firstName,
        ?string $lastName,
        ?bool $isActive
    ) {
        $this->user = $user;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->isActive = $isActive;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }
}
