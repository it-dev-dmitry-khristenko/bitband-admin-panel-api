<?php

declare(strict_types=1);

namespace App\Application\User\UpdateUser;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserRepositoryInterface;
use App\Events\UserDeactivatedEvent;

/**
 * Class UpdateUserHandler
 * @package App\Application\User\UpdateUser
 */
class UpdateUserHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /**
     * UpdateUserHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param Command|UpdateUser $command
     * @return User
     */
    public function handle(Command $command): User
    {
        $user = $command->getUser();

        $user->email = $command->getEmail();
        $user->first_name = $command->getFirstName();
        $user->last_name = $command->getLastName();

        $userActivity = $user->is_active;

        if (is_bool($command->getIsActive())) {
            $user->is_active = $command->getIsActive();
        }

        $this->userRepository->store($user);

        if ($userActivity && !$user->is_active) {
            event(new UserDeactivatedEvent($user));
        }

        return $user;
    }
}
