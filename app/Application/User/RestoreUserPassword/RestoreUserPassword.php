<?php

declare(strict_types=1);

namespace App\Application\User\RestoreUserPassword;

use App\Contract\Core\Command;

/**
 * Class RestoreUserPassword
 * @package App\Application\User\RestoreUserPassword
 */
class RestoreUserPassword implements Command
{
    /**
     * @var string
     */
    private string $code;
    /**
     * @var string
     */
    private string $password;

    /**
     * RestoreUserPassword constructor.
     * @param string $code
     * @param string $password
     */
    public function __construct(
        string $code,
        string $password
    ) {
        $this->code = $code;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
