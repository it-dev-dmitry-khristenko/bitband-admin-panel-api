<?php

declare(strict_types=1);

namespace App\Application\User\RestoreUserPassword;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\User;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * Class RestoreUserPasswordHandler
 * @package App\Application\User\RestoreUserPassword
 */
class RestoreUserPasswordHandler implements Handler
{
    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * RestoreUserPasswordHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param RestoreUserPassword|Command $command
     * @return User
     * @throws ValidationException
     */
    public function handle(Command $command): User
    {
        $filter = new UserFilter();
        $filter->setResetPasswordCode($command->getCode());

        $user = $this->userRepository->filter($filter)->one();

        $now = new Carbon();
        if ($now->gt($user->reset_password_code_expires_at)) {
            throw ValidationException::withMessages([
                "code" => [
                    __('common.reset_password_code_expired')
                ],
            ]);
        }
        if (!$user->is_active) {
            throw ValidationException::withMessages([
                "code" => [
                    __('auth.user_inactive')
                ],
            ]);
        }
        $user->reset_password_code = null;
        $user->reset_password_code_expires_at = null;
        $user->password = Hash::make($command->getPassword());
        $this->userRepository->store($user);

        return $user;
    }
}
