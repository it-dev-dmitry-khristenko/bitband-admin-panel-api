<?php

declare(strict_types=1);

namespace App\Application\User\GetUsersList;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class GetUsersListHandler
 * @package App\Application\User\GetUsersList
 */
class GetUsersListHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /**
     * GetUsersListHandler constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUsersList|Command $command
     * @return LengthAwarePaginator
     */
    public function handle(Command $command): LengthAwarePaginator
    {
        return $this->userRepository->filter($command->getFilter())
            ->sorting($command->getSorting())
            ->paginate($command->getPagination());
    }
}
