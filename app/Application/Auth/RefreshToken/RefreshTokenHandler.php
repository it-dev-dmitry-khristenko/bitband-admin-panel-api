<?php

declare(strict_types=1);

namespace App\Application\Auth\RefreshToken;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Contract\Services\JwtServiceInterface;

/**
 * Class RefreshTokenHandler
 * @package App\Application\Auth\RefreshToken
 */
class RefreshTokenHandler implements Handler
{
    /** @var JwtServiceInterface $jwtService */
    protected JwtServiceInterface $jwtService;

    /**
     * RefreshTokenHandler constructor.
     * @param JwtServiceInterface $jwtService
     */
    public function __construct(JwtServiceInterface $jwtService)
    {
        $this->jwtService = $jwtService;
    }

    /**
     * @param RefreshToken|Command $command
     * @return mixed|void
     */
    public function handle(Command $command): array
    {
        return $this->jwtService->refreshToken($command->getToken());
    }
}
