<?php

declare(strict_types=1);

namespace App\Application\Auth\LoginIntoDashboard;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Contract\Services\JwtServiceInterface;
use App\Domain\AuthToken\AuthToken;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;
use App\Domain\Core\ForbiddenException;
use App\Domain\Role\Role;
use App\Domain\User\UserFilter;
use App\Domain\User\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * Class LoginIntoDashboardHandler
 * @package App\Application\Auth\LoginIntoDashboard
 */
class LoginIntoDashboardHandler implements Handler
{
    /** @var UserRepositoryInterface $userRepository */
    private UserRepositoryInterface $userRepository;

    /** @var JwtServiceInterface $jwtService */
    private JwtServiceInterface $jwtService;
    /**
     * @var AuthTokenRepositoryInterface
     */
    private AuthTokenRepositoryInterface $authTokenRepository;

    /**
     * LoginIntoDashboardHandler constructor.
     * @param UserRepositoryInterface $userRepository
     * @param JwtServiceInterface $jwtService
     * @param AuthTokenRepositoryInterface $authTokenRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        JwtServiceInterface $jwtService,
        AuthTokenRepositoryInterface $authTokenRepository
    ) {
        $this->userRepository = $userRepository;
        $this->jwtService = $jwtService;
        $this->authTokenRepository = $authTokenRepository;
    }

    /**
     * @param LoginIntoDashboard|Command $command
     * @return array
     * @throws ValidationException
     * @throws ForbiddenException
     */
    public function handle(Command $command): array
    {
        $filter = new UserFilter();
        $filter->setEmail($command->getEmail());
        $user = $this->userRepository->filter($filter)->one();

        if (!$user) {
            throw ValidationException::withMessages([
                "email" => [
                    __('auth.incorrect_email_password')
                ],
                "password" => [
                    __('auth.incorrect_email_password')
                ],
            ]);
        }

        if (!Hash::check($command->getPassword(), $user->password)) {
            throw ValidationException::withMessages([
                "email" => [
                    __('auth.incorrect_email_password')
                ],
                "password" => [
                    __('auth.incorrect_email_password')
                ],
            ]);
        }

        if (!$user->roles->contains(Role::ROLE_ADMIN)) {
            throw new ForbiddenException();
        }

        if (!$user->is_active) {
            throw ValidationException::withMessages([
                "email" => [
                    __('auth.user_inactive')
                ],
            ]);
        }

        return $this->jwtService->generateTokenToUser($user);
    }
}
