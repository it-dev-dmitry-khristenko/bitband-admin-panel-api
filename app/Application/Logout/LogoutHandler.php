<?php

declare(strict_types=1);

namespace App\Application\Logout;

use App\Contract\Core\Command;
use App\Contract\Core\Handler;
use App\Domain\AuthToken\AuthTokenRepositoryInterface;

/**
 * Class LogoutHandler
 * @package App\Application\Logout
 */
class LogoutHandler implements Handler
{
    /**
     * @var AuthTokenRepositoryInterface
     */
    private AuthTokenRepositoryInterface $authTokenRepository;

    /**
     * LogoutHandler constructor.
     * @param AuthTokenRepositoryInterface $authTokenRepository
     */
    public function __construct(AuthTokenRepositoryInterface $authTokenRepository)
    {
        $this->authTokenRepository = $authTokenRepository;
    }

    /**
     * @param Command|Logout $command
     * @return void
     */
    public function handle(Command $command): void
    {
        $authToken = $this->authTokenRepository->filter($command->getAuthTokenFilter())
            ->one();

        $this->authTokenRepository->delete($authToken);
    }
}
