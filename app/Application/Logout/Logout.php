<?php

declare(strict_types=1);

namespace App\Application\Logout;

use App\Contract\Core\Command;
use App\Domain\AuthToken\AuthTokenFilter;
use Illuminate\Http\Request;

/**
 * Class Logout
 * @package App\Application\Logout
 */
class Logout implements Command
{
    /**
     * @var AuthTokenFilter
     */
    private AuthTokenFilter $authTokenFilter;

    /**
     * Logout constructor.
     * @param AuthTokenFilter $authTokenFilter
     */
    public function __construct(AuthTokenFilter $authTokenFilter)
    {
        $this->authTokenFilter = $authTokenFilter;
    }

    /**
     * @return AuthTokenFilter
     */
    public function getAuthTokenFilter(): AuthTokenFilter
    {
        return $this->authTokenFilter;
    }
}
