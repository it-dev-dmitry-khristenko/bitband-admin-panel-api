<?php

namespace App\Contract\Core;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;

/**
 * Class AppFileService
 * @package App\Infrastructure\Services
 */
interface AppFileServiceInterface
{
    /**
     * Define default expiration time for temporary url
     * @type int
     */
    public const DEFAULT_EXPIRATION_TIME = 10080;

    /**
     * Define storage driver
     * @type string
     */
    public const DRIVER_CLOUD = 's3';
    /**
     * Define storage driver
     * @type string
     */
    public const DRIVER_LOCAL = 'local';
    /**
     * Define storage driver
     * @type string
     */
    public const DRIVER_PUBLIC = 'public';

    /**
     * @param string $path
     * @param string $source
     * @param int $expirationTime
     * @param string|null $driver
     * @return string
     */
    public function saveString(
        string $path,
        string $source,
        int $expirationTime = self::DEFAULT_EXPIRATION_TIME,
        ?string $driver = null
    ): string;

    /**
     * @param string $path
     * @param UploadedFile $source
     * @param int $expirationTime
     * @param string|null $driver
     * @return string
     */
    public function saveUploadFile(
        string $path,
        UploadedFile $source,
        int $expirationTime = self::DEFAULT_EXPIRATION_TIME,
        ?string $driver = null
    ): string;

    /**
     * @param string $url
     * @param string|null $driver
     */
    public function delete(string $url, ?string $driver = null): void;

    /**
     * @param string $storagePath
     * @param int $expirationTime
     * @param string|null $driver
     * @return string
     */
    public function fullPathByStoragePath(
        string $storagePath,
        int $expirationTime = self::DEFAULT_EXPIRATION_TIME,
        ?string $driver = null
    ): string;

    /**
     * @param string $path
     * @param string|null $driver
     * @return string
     * @throws FileNotFoundException
     */
    public function getFile(string $path, ?string $driver = null): string;

    /**
     * @param string $from
     * @param string $to
     * @param string|null $driver
     */
    public function move(string $from, string $to, ?string $driver = null): void;
}
