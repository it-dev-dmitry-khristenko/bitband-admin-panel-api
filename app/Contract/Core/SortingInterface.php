<?php

declare(strict_types=1);

namespace App\Contract\Core;

use Illuminate\Http\Request;

/**
 * Class SortingInterface
 * @package App\Contract\Core
 */
interface SortingInterface
{
    /**
     * Sort direction
     * @type string
     */
    public const SORT_ASC = 'asc';
    /**
     * Sort direction
     * @type string
     */
    public const SORT_DESC = 'desc';

    /**
     * Define default sort field
     * @type string
     */
    public const DEFAULT_SORT_FIELD = 'created_at';

    /**
     * Define default sort field
     * @type string
     */
    public const DEFAULT_SORT_PARAM = 'sort';

    public static function fromRequest(
        Request $request,
        array $allowedFields,
        ?string $sortParam = self::DEFAULT_SORT_PARAM,
        ?string $defaultSortField = self::DEFAULT_SORT_FIELD
    ): self;

    /**
     * @return string
     */
    public function getField(): string;

    /**
     * @return string
     */
    public function getDirection(): string;

    /**
     * @param string $field
     * @return self
     */
    public function setField($field);

    /**
     * @param string $field
     * @return self
     */
    public function setDirection($field);
}
