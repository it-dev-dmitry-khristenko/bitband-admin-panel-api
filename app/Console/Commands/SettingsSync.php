<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Domain\Settings\SettingsServiceInterface;
use Exception;
use Illuminate\Console\Command;

/**
 * Class SettingsSync
 * @package App\Console\Commands
 */
class SettingsSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'settings:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';
    /**
     * @var SettingsServiceInterface
     */
    private SettingsServiceInterface $settingsService;

    /**
     * SubscriptionProductCreate constructor.
     * @param SettingsServiceInterface $settingsService
     */
    public function __construct(SettingsServiceInterface $settingsService)
    {
        parent::__construct();
        $this->settingsService = $settingsService;
    }

    /**
     * Execute the console command.
     *
     * @return void
     * @throws Exception
     */
    public function handle(): void
    {
        $this->settingsService->syncSettings();

        $this->info('OK');
    }
}
