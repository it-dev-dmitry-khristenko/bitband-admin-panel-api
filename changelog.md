# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
### Update
### Added
- added ability for upload user avatar [ML-16](https://it-devgroup.atlassian.net/browse/ML-16)
- ability for user logout [ML-17](https://it-devgroup.atlassian.net/browse/ML-17)
- ability for assign user card [ML-3](https://it-devgroup.atlassian.net/browse/ML-3)
- ability for add and edit subscription plan [ML-3](https://it-devgroup.atlassian.net/browse/ML-3)
- ability for check webhook [ML-3](https://it-devgroup.atlassian.net/browse/ML-3)
- ability for add user api token [ML-2](https://it-devgroup.atlassian.net/browse/ML-2)
### Changed
- exception handler response
### Deprecated
### Removed
### Fixed
### Security
