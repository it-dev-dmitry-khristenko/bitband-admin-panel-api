<?php

declare(strict_types=1);

namespace Dashboard\Feature;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tests\BaseTestCase;

/**
 * Class DashboardAuthTest
 * @package Tests
 */
class DashboardAuthTest extends BaseTestCase
{
    /**
     * @test
     */
    public function shouldTestDashboardLogin()
    {
        $this->json('POST', route('api.dashboard.auth.login'), [
            'email' => 'test@test.com',
            'password' => 'testpass',
        ])->seeJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'expiresIn',
                    'tokenType'
                ]
            ]
        ])->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldTestDashboardUserInactiveLogin()
    {
        DB::table('users')
            ->where('id', '=', 1)
            ->update([
                'is_active' => false
            ]);

        //fail, user inactive
        $this->json('POST', route('api.dashboard.auth.login'), [
            'email' => 'test@test.com',
            'password' => 'testpass',
        ])
            ->seeJson([
                'errors' => [
                    [
                        'detail' => __('auth.user_inactive'),
                        'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                        'source' => [
                            'parameter' => 'email'
                        ]
                    ],
                ]
            ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        DB::table('users')
            ->where('id', '=', 1)
            ->update([
                'is_active' => true
            ]);

        //success, user active
        $this->json('POST', route('api.dashboard.auth.login'), [
            'email' => 'test@test.com',
            'password' => 'testpass',
        ])->seeJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'expiresIn',
                    'tokenType'
                ]
            ]
        ])->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldBeDashboardLogout()
    {
        $token = $this->loginAsAdmin();

        $this->json('GET', route('api.dashboard.auth.logout'), [], [
            'Authorization' => 'Bearer ' . $token
        ])
        ->seeStatusCode(Response::HTTP_NO_CONTENT);

        $this->json('GET', route('api.dashboard.auth.logout'), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeStatusCode(Response::HTTP_UNAUTHORIZED);
    }


    /**
     * @test
     */
    public function shouldTestDashboardForgotPassword()
    {
        $this->json('POST', route('api.dashboard.auth.forgotPassword'), [
            'email' => 'test@test.com',
        ])->seeJsonStructure([
             'data' => [
                 'type',
             ]
        ])->seeStatusCode(Response::HTTP_OK);

        $user = DB::table('users')->where('email', '=', 'test@test.com')->first();
        $this->assertTrue($user->reset_password_code ? true : false);
    }

    /**
     * @test
     */
    public function shouldTestDashboardUserActivityForgotPassword()
    {
        DB::table('users')
            ->where('id', '=', 1)
            ->update([
                'is_active' => false
            ]);

        $this->json('POST', route('api.dashboard.auth.forgotPassword'), [
            'email' => 'test@test.com',
        ])->seeJson([
            'errors' => [
                [
                    'detail' => __('auth.user_inactive'),
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'source' => [
                        'parameter' => 'email'
                    ]
                ],
            ]
        ])->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function shouldTestUserRestorePassword()
    {
        $this->json('POST', route('api.dashboard.auth.forgotPassword'), [
            'email' => 'test@test.com',
        ])->seeJsonStructure([
             'data' => [
                 'type',
             ]
        ])->seeStatusCode(Response::HTTP_OK);

        $this->json('POST', route('api.dashboard.auth.restorePassword'), [
            'code' => 'expiredPasswordCode',
            'password' => 'superpassword',
            'passwordConfirmation' => 'superpassword',
        ])->seeJson([
            'errors' => [
                [
                    'detail' => __('common.reset_password_code_expired'),
                    'source' => [
                        'parameter' => 'code'
                    ],
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ]
            ]
        ])->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        $user = DB::table('users')->where('email', '=', 'test@test.com')->first();
        $this->json('POST', route('api.dashboard.auth.restorePassword'), [
            'code' => $user->reset_password_code . '1',
            'password' => 'superpassword',
            'passwordConfirmation' => 'superpassword',
        ])->seeJson([
            'errors' => [
                [
                    'detail' => __('validation.exists', [
                        'attribute' => 'code'
                    ]),
                    'source' => [
                        'parameter' => 'code'
                    ],
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ]
            ]
        ])->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        $resetPasswordCode = $user->reset_password_code;
        $this->json('POST', route('api.dashboard.auth.restorePassword'), [
            'code' =>  $resetPasswordCode,
            'password' => 'superpassword',
            'passwordConfirmation' => 'superpassword',
        ])->seeJsonStructure([
             'data' => [
                 'type',
                 'attributes' => [
                     'accessToken',
                     'expiresIn',
                     'tokenType'
                 ]
             ]
        ])->seeStatusCode(Response::HTTP_OK);

        $user = DB::table('users')->where('email', '=', 'test@test.com')->first();
        Hash::check('superpassword', $user->password);
        $this->assertTrue(Hash::check('superpassword', $user->password));

        $this->json('POST', route('api.dashboard.auth.restorePassword'), [
            'code' =>  $resetPasswordCode,
            'password' => 'superpassword',
            'passwordConfirmation' => 'superpassword',
        ])->seeJson([
            'errors' => [
                [
                    'detail' => __('validation.exists', [
                        'attribute' => 'code'
                    ]),
                    'source' => [
                        'parameter' => 'code'
                    ],
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                ]
            ]
        ])->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function shouldTestDashboardDeactivatedUserRestorePassword()
    {
        $this->json('POST', route('api.dashboard.auth.forgotPassword'), [
            'email' => 'test@test.com',
        ])->seeJsonStructure([
            'data' => [
                'type',
            ]
        ])->seeStatusCode(Response::HTTP_OK);

        DB::table('users')
            ->where('id', '=', 1)
            ->update([
                'is_active' => false
            ]);

        $user = DB::table('users')->where('email', '=', 'test@test.com')->first();
        $resetPasswordCode = $user->reset_password_code;
        $this->json('POST', route('api.dashboard.auth.restorePassword'), [
            'code' =>  $resetPasswordCode,
            'password' => 'superpassword',
            'passwordConfirmation' => 'superpassword',
        ])->seeJson([
            'errors' => [
                [
                    'detail' => __('auth.user_inactive'),
                    'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    'source' => [
                        'parameter' => 'code'
                    ]
                ],
            ]
        ])->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        DB::table('users')
            ->where('id', '=', 1)
            ->update([
                'is_active' => true
            ]);

        $this->json('POST', route('api.dashboard.auth.restorePassword'), [
            'code' =>  $resetPasswordCode,
            'password' => 'superpassword',
            'passwordConfirmation' => 'superpassword',
        ])->seeJsonStructure([
            'data' => [
                'type',
                'attributes' => [
                    'accessToken',
                    'expiresIn',
                    'tokenType'
                ]
            ]
        ])->seeStatusCode(Response::HTTP_OK);
    }
}
