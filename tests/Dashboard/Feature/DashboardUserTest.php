<?php

declare(strict_types=1);

namespace Dashboard\Feature;

use App\Domain\Role\Role;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Tests\BaseTestCase;

/**
 * Class DashboardAuthTest
 * @package Tests
 */
class DashboardUserTest extends BaseTestCase
{
    /**
     * @test
     */
    public function shouldTestDashboardUserListing()
    {
        $token = $this->loginAsAdmin();
        $this->json('GET', route('api.dashboard.users.index'), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    [
                        'type',
                        'attributes' => [
                            'email',
                            'firstName',
                            'lastName',
                            'isActive',
                            'createdAt',
                            'updatedAt',
                        ]
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems',
                ]
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }



    /**
     * @test
     */
    public function shouldTestSearchUserListing()
    {
        $token = $this->loginAsAdmin();
        $this->json('GET', route('api.dashboard.users.index'), [
            'searchQuery' => 'te'
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    [
                        'type',
                        'attributes' => [
                            'email',
                            'firstName',
                            'lastName',
                            'createdAt',
                            'updatedAt',
                        ]
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems',
                ]
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldTestFilterByStatusUserListing()
    {
        $token = $this->loginAsAdmin();
        //filter by inactive, users count 0, because all users is active by default
        $this->json('GET', route('api.dashboard.users.index'), [
            'status' => 0
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJson([
                'meta' => [
                    'totalPages' => 0,
                    'totalItems' => 0,
                ]
            ])
            ->seeStatusCode(Response::HTTP_OK);

        DB::table('users')
            ->where('id', '=', 4)
            ->update([
                'is_active' => false
            ]);

        //filter by inactive, users count 1
        $this->json('GET', route('api.dashboard.users.index'), [
            'status' => 0
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJson([
                'meta' => [
                    'totalPages' => 1,
                    'totalItems' => 1,
                ]
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldTestShowUserById()
    {
        $token = $this->loginAsAdmin();
        $this->json('GET', route('api.dashboard.users.show', ['id' => 1]), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'createdAt',
                        'updatedAt',
                    ]
                ],
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldTestUserStore()
    {
        $token = $this->loginAsAdmin();
        $this->json('POST', route('api.dashboard.users.store', []), [
            'email' => 'just@do.it',
            'firstName' => 'third',
            'lastName' => 'admin',
            'roleIds' => [Role::ROLE_ADMIN],
            'isActive' => false
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    'id',
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'isActive',
                        'createdAt',
                        'updatedAt',
                    ]
                ],
            ])
            ->seeStatusCode(Response::HTTP_CREATED);

        $this->seeInDatabase('users', [
            'email' => 'just@do.it',
            'first_name' => 'third',
            'last_name' => 'admin',
            'is_active' => false
        ]);
    }

    /**
     * @test
     */
    public function shouldTestEmptyNameStoreUser()
    {
        $token = $this->loginAsAdmin();
        $this->json('POST', route('api.dashboard.users.store', []), [
            'email' => 'just@do.it',
            'roleIds' => [Role::ROLE_ADMIN]
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    'id',
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'createdAt',
                        'updatedAt',
                    ]
                ],
            ])
            ->seeStatusCode(Response::HTTP_CREATED);

        $this->seeInDatabase('users', [
            'email' => 'just@do.it',
            'first_name' => null,
            'last_name' => null
        ]);
    }

    /**
     * @test
     */
    public function shouldTestUpdateUserWithActivity()
    {
        $token = $this->loginAsAdmin();

        $this->seeInDatabase('users', [
            'id' => 4,
            'email' => 'user@test.com',
            'is_active' => true
        ]);

        $userAuthToken = DB::table('auth_tokens')
            ->where('user_id', '=', 4)
            ->first();

        $this->json('PUT', route('api.dashboard.users.update', ['id' => 4]), [
            'email' => 'just@do.it',
            'firstName' => 'update',
            'lastName' => 'admin',
            'isActive' => false
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'isActive',
                        'createdAt',
                        'updatedAt',
                    ]
                ],
            ])
            ->seeStatusCode(Response::HTTP_OK);

        $this->seeInDatabase('users', [
            'id' => 4,
            'email' => 'just@do.it',
            'first_name' => 'update',
            'last_name' => 'admin',
            'is_active' => false
        ]);

        //token revoked, we can't get data with this token anymore
        $this->notSeeInDatabase('auth_tokens', [
            'id' => $userAuthToken->id,
        ]);
    }

    /**
     * @test
     */
    public function shouldTestUserCantDeactivateHimself()
    {
        $token = $this->loginAsAdmin();
        $this->json('PUT', route('api.dashboard.users.update', ['id' => 1]), [
            'email' => 'just@do.it',
            'firstName' => 'update',
            'lastName' => 'admin',
            'isActive' => false
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJson([
                'errors' => [
                    [
                        'detail' => __('auth.user_cant_deactivate_himself'),
                        'status' => Response::HTTP_FORBIDDEN,
                    ],
                ],
            ])
            ->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function shouldTestDestroyUser()
    {
        $token = $this->loginAsAdmin();

        $this->seeInDatabase('users', [
            'id' => 2,
            'email' => 'super@mlearning.com',
            'first_name' => 'firstname',
            'last_name' => 'lastname',
        ]);

        $this->seeInDatabase('user_roles', [
            'user_id' => 2,
            'role_id' => Role::ROLE_ADMIN
        ]);

        $this->json('DELETE', route('api.dashboard.users.destroy', ['id' => 2]), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeStatusCode(Response::HTTP_NO_CONTENT);

        $this->missingFromDatabase('users', [
            'id' => 2,
            'email' => 'super@mlearning.com',
            'first_name' => 'firstname',
            'last_name' => 'lastname',
        ]);

        $this->missingFromDatabase('user_roles', [
            'user_id' => 2,
            'role_id' => Role::ROLE_ADMIN
        ]);
    }

    /**
     * @test
     */
    public function shouldTestUserCantDestroyHimself()
    {
        $token = $this->loginAsAdmin();

        $this->json('DELETE', route('api.dashboard.users.destroy', ['id' => 1]), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJson([
                'errors' => [
                    [
                        'detail' => __('auth.user_cant_delete_himself'),
                        'status' => Response::HTTP_FORBIDDEN,
                    ],
                ],
            ])
            ->seeStatusCode(Response::HTTP_FORBIDDEN);
    }

    /**
     * @test
     */
    public function shouldChangeUserPassword()
    {
        $token = $this->loginAsAdmin();

        $this->json('put', route('api.dashboard.users.change-password', ['id' => 4]), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'createdAt',
                        'updatedAt',
                    ]
                ],
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }
}
