<?php

declare(strict_types=1);

namespace Dashboard\Feature;

use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tests\BaseTestCase;

/**
 * Class DashboardProfileTest
 * @package Dashboard\Feature
 */
class DashboardProfileTest extends BaseTestCase
{
    /**
     * @test
     */
    public function shouldTestGetDashboardUserProfile()
    {
        $token = $this->loginAsAdmin();
        $this->json(
            'GET',
            route('api.dashboard.user.profile'),
            [],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )->seeJsonStructure(
            [
                'data' => [
                    'id',
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'createdAt',
                        'updatedAt',
                    ],
                    'relationships' => [
                        'avatar',
                        'roles'
                    ]
                ]
            ]
        )
            ->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldTestUpdateDashboardUserProfile()
    {
        $token = $this->loginAsAdmin();

        $this->json(
            'PUT',
            route('api.dashboard.user.updateProfile'),
            [
                'firstName' => 'first',
                'lastName' => 'last',
            ],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )->seeJsonStructure(
            [
                'data' => [
                    'id',
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'createdAt',
                        'updatedAt',
                    ]
                ]
            ]
        )
            ->seeStatusCode(Response::HTTP_OK);

        $this->seeInDatabase(
            'users',
            [
                'id' => 1,
                'first_name' => 'first',
                'last_name' => 'last',
            ]
        );
    }

    /**
     * @test
     */
    public function shouldTestUpdateDashboardUserPassword()
    {
        $token = $this->loginAsAdmin();

        $this->json(
            'PUT',
            route('api.dashboard.user.updatePassword'),
            [
                'currentPassword' => 'testpass1',
                'password' => 'superpassword',
                'passwordConfirmation' => 'superpassword',
            ],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )->seeJson(
            [
                'errors' => [
                    [
                        'detail' => __('common.current_password_incorrect'),
                        'source' => [
                            'parameter' => 'currentPassword'
                        ],
                        'status' => Response::HTTP_UNPROCESSABLE_ENTITY,
                    ]
                ]
            ]
        )
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->json(
            'PUT',
            route('api.dashboard.user.updatePassword'),
            [
                'currentPassword' => 'testpass',
                'password' => 'superpassword',
                'passwordConfirmation' => 'superpassword',
            ],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )->seeJsonStructure(
            [
                'data' => [
                    'id',
                    'type',
                    'attributes' => [
                        'email',
                        'firstName',
                        'lastName',
                        'createdAt',
                        'updatedAt',
                    ],
                    'relationships' => [
                        'avatar',
                        'roles'
                    ]
                ]
            ]
        )
            ->seeStatusCode(Response::HTTP_OK);

        $user = DB::table('users')->where('email', '=', 'test@test.com')->first();
        Hash::check('superpassword', $user->password);
        $this->assertTrue(Hash::check('superpassword', $user->password));
    }

    /**
     * @test
     */
    public function shouldDashboardUploadAvatar()
    {
        $token = $this->loginAsAdmin();

        $res = $this->call(
            'POST',
            route('api.dashboard.user.upload-avatar'),
            [],
            [],
            [
                'avatar' => UploadedFile::fake()->image('avatar.jpg', 100, 100)->size(100)
            ],
            [
                'HTTP_AUTHORIZATION' => 'Bearer ' . $token
            ]
        );
        $this->assertEquals(
            [
                'data' => [
                    'type' => 'file',
                    'id' => 2
                ]
            ],
            json_decode($res->content(), true)
        );
        $this->assertEquals(Response::HTTP_CREATED, $res->status());
    }

    /**
     * @test
     */
    public function shouldDeleteDashboardUserAvatar()
    {
        $token = $this->loginAsAdmin();

        $res = $this->call(
            'DELETE',
            route('api.dashboard.user.delete-avatar'),
            [],
            [],
            [],
            [
                'HTTP_AUTHORIZATION' => 'Bearer ' . $token
            ]
        );

        $this->assertEquals(Response::HTTP_NO_CONTENT, $res->status());
    }
}
