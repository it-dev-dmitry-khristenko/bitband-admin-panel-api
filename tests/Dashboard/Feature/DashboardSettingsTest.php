<?php

declare(strict_types=1);

namespace Dashboard\Feature;

use Illuminate\Http\Response;
use Tests\BaseTestCase;

/**
 * Class DashboardSettingsTest
 * @package Dashboard\Feature
 */
class DashboardSettingsTest extends BaseTestCase
{
    /**
     * @test
     */
    public function shouldReturnSettingsGroupList()
    {
        $token = $this->loginAsAdmin();

        $this->json(
            'get',
            route('api.dashboard.settings.groups'),
            [],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )
            ->seeJsonStructure([
                'data' => [
                    [
                        'type',
                        'attributes' => [
                            'key',
                            'title',
                            'order'
                        ]
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems'
                ]
            ]);
    }

    /**
     * @test
     */
    public function shouldReturnSettingsList()
    {
        $token = $this->loginAsAdmin();

        $this->json(
            'get',
            route('api.dashboard.settings.get', [
                'key' => 'main'
            ]),
            [],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )
            ->seeJsonStructure([
                'data' => [
                    [
                        'type',
                        'attributes' => [
                            'key',
                            'title',
                            'value',
                            'group',
                            'description',
                            'type',
                            'option',
                            'isSecret',
                            'validationRules',
                        ]
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems'
                ]
            ]);
    }

    /**
     * @test
     */
    public function shouldUpdateSettings()
    {
        $token = $this->loginAsAdmin();

        $this->json(
            'put',
            route('api.dashboard.settings.update', [
                'key' => 'main_pagination_per_page'
            ]),
            [
                'value' => '25'
            ],
            [
                'Authorization' => 'Bearer ' . $token
            ]
        )
            ->assertResponseStatus(Response::HTTP_NO_CONTENT);

        $this->seeInDatabase('settings', [
            'value' => '25',
            'key' => 'main_pagination_per_page'
        ]);
    }
}
