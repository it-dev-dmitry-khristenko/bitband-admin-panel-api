<?php

declare(strict_types=1);

namespace Dashboard\Feature;

use Illuminate\Http\Response;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Tests\BaseTestCase;

/**
 * Class DashboardAuthTest
 * @package Tests
 */
class DashboardRoleTest extends BaseTestCase
{
    /**
     * @test
     */
    public function shouldTestDashboardRoleListing()
    {
        $token = $this->loginAsAdmin();
        $this->json('GET', route('api.dashboard.roles.index'), [], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    [
                        'type',
                        'attributes' => [
                            'title',
                            'createdAt',
                            'updatedAt',
                        ]
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems',
                ]
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function shouldTestSearchRoleListing()
    {
        $token = $this->loginAsAdmin();
        $this->json('GET', route('api.dashboard.roles.index'), [
            'searchQuery' => 'ad'
        ], [
            'Authorization' => 'Bearer ' . $token
        ])
            ->seeJsonStructure([
                'data' => [
                    [
                        'type',
                        'attributes' => [
                            'title',
                            'createdAt',
                            'updatedAt',
                        ]
                    ]
                ],
                'meta' => [
                    'totalPages',
                    'totalItems',
                ]
            ])
            ->seeStatusCode(Response::HTTP_OK);
    }
}
