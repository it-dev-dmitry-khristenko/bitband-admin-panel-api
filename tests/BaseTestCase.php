<?php

declare(strict_types=1);

namespace Tests;

use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\TestCase;
use PHPUnit\Framework\TestResult;

/**
 * Class BaseTestCase
 * @package Tests
 */
abstract class BaseTestCase extends TestCase
{
    /**
     * @param TestResult|null $result
     * @return TestResult
     */
    public function run(TestResult $result = null): TestResult
    {
        return parent::run($result);
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }

    /**
     * Initial setup for tests
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
        Artisan::call('settings:sync');
        Artisan::call('db:seed');
    }

    /**
     * @return string
     */
    protected function loginAsAdmin()
    {
        $data = json_decode($this->json('POST', route('api.dashboard.auth.login'), [
            'email' => 'test@test.com',
            'password' => 'testpass',
        ])->response->content(), false);

        return $data->data->attributes->accessToken;
    }
}
