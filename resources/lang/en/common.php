<?php

declare(strict_types=1);

return [
    'reset_password_code_expired' => 'Reset password code expired',
    'current_password_incorrect' => 'Current password incorrect',
    'user_already_has_payment_method' => 'User already has linked payment method.',
    'user_does_not_have_payment_method' => 'User does not have a payment method',
    'face_detection_failed' => 'Something went wrong on face detection, please try again',
    'api_token' => [
        'never_accessed' => 'Never accessed'
    ]
];
