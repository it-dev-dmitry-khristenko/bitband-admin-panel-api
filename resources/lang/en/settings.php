<?php

declare(strict_types=1);

return [
    'boolean' => [
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'containers' => [
        'main' => 'Main',
        'stripe' => 'Stripe',
        'free-calls' => 'User free API calls'
    ]
];
