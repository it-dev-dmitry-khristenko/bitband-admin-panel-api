<?php

declare(strict_types=1);

return [
    'payment_card' => [
        'delete' => 'Last payment card cannot be deleted',
        'not_define' => 'User payment card not defined'
    ],
    'subscription' => [
        'not_define' => 'User subscription not defined',
        'not_paid' => 'Last invoice didn\'t pay',
        'already_subscribed' => 'You are already subscribed'
    ],
    'payment_method' => [
        'not_found' => 'Payment method with id :id not found',
        'exists' => 'User has already payment method',
    ],
    'api_token' => [
        'owner' => 'User don\'t has permission for delete api token',
    ],
    'settings' => [
        'not_found' => 'Settings with key :key not found',
    ],
    'free_calls' => [
        'usage_has_exceeded_free_tier' => 'Usage has exceeded free tier. Please upgrade <a href=":billingPage">Billing page</a>',
    ],
    'app_calls' => [
        'app_calls_limit_exceeded' => 'Application calls limit exceeded',
    ]
];
