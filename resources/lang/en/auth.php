<?php

declare(strict_types=1);

return [
    'unauthenticated' => 'Unauthorized',
    'incorrect_email_password' => 'Incorrect email or password',
    'forbidden' => 'Forbidden',
    'user_inactive' => 'User inactive',
    'user_cant_deactivate_himself' => 'User can\'t deactivate himself',
    'user_cant_delete_himself' => 'User can\'t delete himself',
];
