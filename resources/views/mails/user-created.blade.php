Hello dear {{$role}}, administrator created an account for you. <br>
Please follow this link to activate it <a href="{{$uiUrl}}">{{$uiUrl}}</a> <br>
Use your email {{$email}} and password {{$password}} for login. <br>
Please change your password after login.
