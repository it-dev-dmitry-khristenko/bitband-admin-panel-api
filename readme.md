#Project installation

1. copy .env.example to .env ` cp .env.example .env`
2. run `make` in command line
4. run `docker-compose exec php-fpm php artisan settings:sync` always when run deploy
3. run `docker-compose exec php-fpm php artisan db:seed` only once
5. run `docker-compose exec php-fpm php artisan subscription-product:create` only once
6. run `docker-compose exec php-fpm /bin/sh -c '/usr/bin/supervisorctl restart all'` before finished deploy
6. Email messages available on address http://localhost:8725/

##STRIPE 
After register in stripe you need make webhooh on Stripe dashboard Developers -> Webhooks

Endpoint for add in endpoint `/api/webhook/stripe`
And copy `Signing secret` in `.env`


#Google Cloud Vision

To init google cloud vision, json file should be in project root + .env should have GOOGLE_APPLICATION_CREDENTIALS variable. 

For example: GOOGLE_APPLICATION_CREDENTIALS=GoogleVision.json.  


#Supervisor panel
http://0.0.0.0:9006/

login: test@test.com

password: ntgkjdjp


#RabbitMQ panel
http://0.0.0.0:15672/

login: rabbitmq

password: rabbitmq



