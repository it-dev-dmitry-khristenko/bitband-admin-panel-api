<?php

declare(strict_types=1);

return [
    'dev_environments' => explode(',', env('DEV_ENVIRONMENTS')),
    'ui_url' => env('UI_URL'),
    'ui_port' => env('UI_PORT'),
    'ui_dashboard_url' => env('UI_DASHBOARD_URL'),
    'ui_dashboard_port' => env('UI_DASHBOARD_PORT'),
    'locale' => env('APP_LOCALE'),
    'swagger_host' => env('SWAGGER_HOST'),
    'swagger_base_path' => env('SWAGGER_BASE_PATH'),
    'swagger_schemes' => env('SWAGGER_SCHEMES') ? explode(',', env('SWAGGER_SCHEMES')) : null,
];
