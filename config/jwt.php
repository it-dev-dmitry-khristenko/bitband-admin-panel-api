<?php
return [
    'app_key' => env('APP_KEY'),
    'token_lifetime' => env('TOKEN_LIFETIME'),
];
